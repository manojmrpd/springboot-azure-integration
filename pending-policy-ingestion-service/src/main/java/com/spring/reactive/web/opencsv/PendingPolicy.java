package com.spring.reactive.web.opencsv;

import com.opencsv.bean.CsvBindByPosition;
import com.spring.reactive.web.constant.PendingPolicyConstant;
import com.spring.reactive.web.validation.DateValidator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;
import java.time.LocalDate;

import static com.spring.reactive.web.constant.PendingPolicyConstant.*;

@Getter
@Setter
@ToString(of = "applicationNumber")
public abstract class PendingPolicy {
    @CsvBindByPosition(position = 1)
    @Max(THREE_DIGITS)
    @NotNull
    @Positive
    private Integer productFamilyCodeStandard1;
    @CsvBindByPosition(position = 2)
    @Max(THREE_DIGITS)
    @NotNull
    @Positive
    private Integer productFamilyCodeStandard2;
    @CsvBindByPosition(position = 3)
    @Max(SEVEN_DIGITS)
    @NotNull
    @Positive
    private Integer policyNumber;
    @CsvBindByPosition(position = 4)
    @Max(ELEVEN_DIGITS)
    @NotNull
    @Positive
    private Long applicationNumber;
    @CsvBindByPosition(position = 5)
    @NotBlank
    @Size(max = SIX)
    private String planCode;
    @CsvBindByPosition(position = 6)
    @Max(SIX_DIGITS)
    @NotNull
    @Positive
    private Integer agentCode;
    @CsvBindByPosition(position = 7)
    @NotBlank
    @Size(max = TWO)
    private String premiumMode;
    @CsvBindByPosition(position = 9)
    @DateValidator
    @NotBlank
    @Size(max = 8)
    private String applicationDate;
    @CsvBindByPosition(position = 10)
    @NotBlank
    private String nameKanji;
    @CsvBindByPosition(position = 11)
    @NotBlank
    private String nameKana;
    @CsvBindByPosition(position = 12)
    @Pattern(regexp = DATE_REGEX)
    @NotNull
    @Size(max = 8)
    private String dateOfBirth;
    @CsvBindByPosition(position = 13)
    @Max(9)
    @NotNull
    @Positive
    private Integer gender;
    @CsvBindByPosition(position = 14)
    @Size(max = 8)
    private String addressCode;
    @CsvBindByPosition(position = 15)
    @Size(max = 150)
    private String addressDetail;
    @CsvBindByPosition(position = 16)
    @NotBlank
    @Size(max = 3)
    private String currencyCode;
    @CsvBindByPosition(position = 17)
    @NotBlank
    @Size(max = 10)
    private String ttmRate;
    @CsvBindByPosition(position = 18)
    @NotBlank
    @Size(max = 19)
    private String lumpSumPremiumsTtmRate;
    @CsvBindByPosition(position = 19)
    @NotBlank
    @Size(max = 19)
    private String policyHolderFinancialAssets;
    @CsvBindByPosition(position = 20)
    @NotBlank
    @Size(max = 19)
    private String policyHolderAnnualIncome;
    private LocalDate cancellationDate;
    private String nameKanaConverted;
}