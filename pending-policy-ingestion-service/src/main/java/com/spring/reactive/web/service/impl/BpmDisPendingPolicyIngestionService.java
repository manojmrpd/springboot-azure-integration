package com.spring.reactive.web.service.impl;

import com.spring.reactive.web.config.property.ConfigurationPropertiesConfig;
import com.spring.reactive.web.document.PendingPolicyDocument;
import com.spring.reactive.web.repository.PendingPolicyRepository;
import com.spring.reactive.web.validation.document.ConstraintViolationUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class BpmDisPendingPolicyIngestionService {
    private final PendingPolicyRepository pendingPolicyRepository;
    private final ConfigurationPropertiesConfig configurationPropertiesConfig;
    private final ConstraintViolationUtil constraintViolation;

    public Flux<PendingPolicyDocument> processBpmAndDis() {
        log.info("processing bpm and dis source pending policies");
        Set<String> sources =
                configurationPropertiesConfig.getSources().parallelStream()
                        .filter(source -> source.equals("BPM") && source.equals("DIS")).collect(Collectors.toSet());
        return pendingPolicyRepository.findByDataSourceIn(sources)
                .flatMap(constraintViolation::validate).log();
    }
}
