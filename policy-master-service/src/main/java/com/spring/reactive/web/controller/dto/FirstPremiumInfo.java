package com.spring.reactive.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class FirstPremiumInfo {
    @JsonProperty("receiptNumber")
    private String receiptNumber;
    @JsonProperty("receiptDate")
    private String receiptDate;
    @JsonProperty("firstPremiumAmount")
    private String firstPremiumAmount;
    @JsonProperty("firstPremiumPaymentCurrencyCode")
    private String firstPremiumPaymentCurrencyCode;
}