package com.spring.reactive.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.math.BigDecimal;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@ToString
public class Fund {
    @JsonIgnore
    private String id;
    @JsonIgnore
    private String policyNo;
    @JsonIgnore
    private Boolean isDeleted;
    private String name;
    private String code;
    private String integrationKey;
    private AccumulatedBalance accumulatedBalance;
    private SubFund subFund;
    private String insurerId;
    private BigDecimal totalBalance;
    private String scId;
    private String specialAccountRatio;
    private String allocationAmount;
    private String freeTransferRemaining;
    private String productFamilyCodeStandard1;
    private String productFamilyCodeStandard2;
    @JsonProperty("policyNumber")
    private Integer policyNumber;
    private String coverageNumber;
}
