package com.spring.reactive.web.service.impl;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.spring.reactive.web.service.util.AppliedPolicyServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.reactive.web.dto.ApplicationInfo;
import com.spring.reactive.web.dto.Common;
import com.spring.reactive.web.dto.ElderlyCheckRequest;
import com.spring.reactive.web.dto.ElderlyCheckResponse;
import com.spring.reactive.web.dto.HistoryCheckRequest;
import com.spring.reactive.web.dto.HistoryCheckResponse;
import com.spring.reactive.web.service.HistoryCheckService;
import com.spring.reactive.web.service.util.EnforcedPolicyServiceUtil;
import com.spring.reactive.web.service.util.PendingPolicyServiceUtil;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class HistoryCheckServiceImpl implements HistoryCheckService {

    @Autowired
    private PendingPolicyServiceUtil pendingPolicyServiceUtil;

    @Autowired
    private EnforcedPolicyServiceUtil enforcedPolicyServiceUtil;
    @Autowired
    private AppliedPolicyServiceUtil appliedPolicyServiceUtil;

    @Override
    public Mono<HistoryCheckResponse> historyCheck(Mono<HistoryCheckRequest> historyCheckRequest) {
        historyCheckRequest.flatMap(request -> getAllElderlyPolicies(request)).subscribe();
        return null;
    }

    private Mono<Set<Common>> getAllElderlyPolicies(HistoryCheckRequest historyCheckRequest) {
        return pendingPolicyServiceUtil.getPendingPolicies(historyCheckRequest).log()
                .mergeWith(enforcedPolicyServiceUtil.getEnforcedPolicies(historyCheckRequest).log()
                        .mergeWith(appliedPolicyServiceUtil.getAppliedPolicies(historyCheckRequest.getElderlyCheckRequest().getApplicationInfo()))).log()
                .collect(Collectors.toSet());
    }

    private Function<Set<Common>, ElderlyCheckResponse> generateElderlyCheckResponse(
            ElderlyCheckRequest elderlyCheckRequest, Set<ApplicationInfo> applicationInfoSet) {
        return null;
    }

    public static Function<ElderlyCheckResponse, Mono<HistoryCheckResponse>> generateHistoryCheckResponse() {
        return (final ElderlyCheckResponse elderlyCheckResponse) -> Mono.just(
                HistoryCheckResponse.builder().elderlyCheckResponse(elderlyCheckResponse).build());
    }

}
