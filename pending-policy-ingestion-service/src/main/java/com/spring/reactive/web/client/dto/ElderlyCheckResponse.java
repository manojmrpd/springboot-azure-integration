package com.spring.reactive.web.client.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ElderlyCheckResponse {
	private String applicationNumber;
	private BigDecimal annualIncome;
	private BigDecimal financialAssets;
	private BigDecimal totalAnnualPremium;
	private BigDecimal estimatedTotalPremium;
	private BigDecimal annualIncomelimitForLevelPayment;
	private BigDecimal financialAssetsLimitForLevelPayment;
	private BigDecimal totalLumpSumpPremium;
	private BigDecimal financialAssetsLimitForLumpSumPayment;
	private BigDecimal excessAnnualIncomeForLevelPayment;
	private BigDecimal excessFinancialAssetsForLevelPayment;
	private BigDecimal excessFinancialAssetsForLumpSumPayment;
	private Long policyCountForOneYear;
	private Long policyCountForTwoYear;
	private Long policyCountForLevelPayment;
	private Long excessPolicyCountForOneYear;
	private Long excessPolicyCountForTwoYear;
	private Long excessPolicyCountForLevelPayment;
	private List<CheckResult> checkResult;
	private Integer systemErrorCode;
	private String systemErrorMessage;
	

}
