package com.spring.reactive.web.service.csv;

import com.spring.reactive.web.document.PendingPolicyDocument;
import org.springframework.stereotype.Service;

@Service
public interface CsvWriterService {
    void writeBpmAndDisSource(PendingPolicyDocument document, String message);
}
