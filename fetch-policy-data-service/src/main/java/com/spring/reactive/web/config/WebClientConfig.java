package com.spring.reactive.web.config;

import com.spring.reactive.web.client.config.PolicyMasterConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Data
public class WebClientConfig {
    @Autowired
    private PolicyMasterConfig policyMasterConfig;

    @Bean
    public WebClient policyMasterWebClient() {
        return WebClient.builder().baseUrl(policyMasterConfig.getUri()).build();

    }
}
