package com.spring.reactive.web.dto;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class HistoryCheckResponse {
	private ElderlyCheckResponse elderlyCheckResponse;

}
