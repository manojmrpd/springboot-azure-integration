package com.spring.reactive.web.repository.document;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Builder;
import lombok.Data;

import static com.spring.reactive.web.constant.HistoryCheckConstant.*;

@Data
@Builder
@Document(collection = "pending_policies")
public class PendingPolicyDocument {
		@Id
		private BigInteger id;
		private Integer productFamilyCodeStandard1;
		private Integer productFamilyCodeStandard2;
		private Integer policyNumber;
		private Long applicationNumber;
		private String planCode;
		private Integer agentCode;
		private String premiumMode;
		private String premiumPayPeriod;
		private String applicationDate;
		@Field(PEND_POLICY_CLIENTS)
		private Set<PendingPolicyClientDocument> pendingPolicyClients;
		@Field(POLICYHOLDER_ANNUAL_INCOME)
		private String policyHolderAnnualIncome;
		@Field(POLICYHOLDER_FINANCIAL_ASSETS)
		private String policyHolderFinancialAssets;
		@Field(TOTAL_ANNUAL_PREMIUMS_TTMRATE)
		private String totalAnnualPremiumsTtmRate;
		@Field(ESTIMATED_TOTAL_PREMIUMS_TTMRATE)
		private String estimatedTotalPremiumsTtmRate;
		@Field(LUMP_SUM_PREMIUMS_TTMRATE)
		private String lumpSumPremiumsTtmRate;
		private String currencyCode;
		@Field(TTMRATE)
		private String ttmRate;
		private LocalDate effectiveDate;
		private LocalDate cancellationDate;
		private LocalDate updateDate;
		private Integer premiumMethod;
		@Field(LEVEL_ORLUMPSUM_PREMIUM)
		private Long levelOrLumpSumPremium;
		@Field(POLICY_ISSUED_FLG)
		private String policyIssued;
		private String dataSource;
}
