package com.spring.reactive.web.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.spring.reactive.web.service.util.RateMasterConverterUtil;
import org.springframework.stereotype.Service;

import com.spring.reactive.web.client.dto.ApplicationInfo;
import com.spring.reactive.web.client.dto.ElderlyCheckRequest;
import com.spring.reactive.web.client.dto.ElderlyCheckResponse;
import com.spring.reactive.web.service.dto.Common;
import com.spring.reactive.web.service.dto.EnforcedPolicyInfo;
import com.spring.reactive.web.service.dto.PaymentModeConstant;
import com.spring.reactive.web.service.dto.PendingPolicyInfo;
import com.spring.reactive.web.service.dto.PolicyTypeConstant;

import reactor.core.publisher.Mono;

@Service
public class HistoryCheckServiceImpl {

	private RateMasterConverterUtil rateMasterUtil = new RateMasterConverterUtil();

	public static void main(String[] args) {
		HistoryCheckServiceImpl serviceImpl = new HistoryCheckServiceImpl();
		serviceImpl
				.historyCheckService(ElderlyCheckRequest.builder().nameKana("Test").birthDate(LocalDate.now()).build());
	}

	Mono<ElderlyCheckResponse> historyCheckService(ElderlyCheckRequest request) {
		Set<Common> commonSet = buildCommon();
		generateElderlyCheckResponse(request, request.getApplicationInfo(), commonSet);
		return null;

	}

	Set<Common> buildCommon() {
		Set<Common> set = new HashSet<>();
		Common one = Common.builder().paymentModeConstant(PaymentModeConstant.LEVEL)
				.policyTypeConstant(PolicyTypeConstant.ENFORCED).pendingPolicyInfo(null)
				.enforcedPolicyInfo(EnforcedPolicyInfo.builder().nameKana("TEST").baseDate(LocalDate.now())
						.dob("1951-11-02").gender(1).issueDate(LocalDate.now()).totalAnnualPremiums("1200")
						.lumpSumPremiums("20000").premiumMode("99").build())
				.currencyPay("JPY").source("VAN").build();

		Common two = Common.builder().paymentModeConstant(PaymentModeConstant.LUMP_SUM)
				.policyTypeConstant(PolicyTypeConstant.ENFORCED).pendingPolicyInfo(null)
				.enforcedPolicyInfo(EnforcedPolicyInfo.builder().nameKana("TEST").baseDate(LocalDate.now())
						.dob("1951-11-02").gender(1).issueDate(LocalDate.now()).totalAnnualPremiums("1200")
						.lumpSumPremiums("20000").premiumMode("99").build())
				.currencyPay("USD").source("VAN").build();

		Common three = Common.builder().paymentModeConstant(PaymentModeConstant.LEVEL)
				.policyTypeConstant(PolicyTypeConstant.PENDING)
				.pendingPolicyInfo(PendingPolicyInfo.builder().premiumMode("3").totalAnnualPremiums("3000")
						.estimatedTotalPremiums("25000").lumpSumPremiums("12000").build())
				.enforcedPolicyInfo(null).currencyPay("EUR").source("ING").build();

		Common four = Common.builder().paymentModeConstant(PaymentModeConstant.LUMP_SUM)
				.policyTypeConstant(PolicyTypeConstant.PENDING)
				.pendingPolicyInfo(PendingPolicyInfo.builder().premiumMode("3").totalAnnualPremiums("3000")
						.estimatedTotalPremiums("25000").lumpSumPremiums("12000").build())
				.enforcedPolicyInfo(null).currencyPay("JPY").source("ING").build();

		Common five = Common.builder().paymentModeConstant(PaymentModeConstant.LEVEL)
				.policyTypeConstant(PolicyTypeConstant.PENDING)
				.pendingPolicyInfo(PendingPolicyInfo.builder().premiumMode("3").totalAnnualPremiums("3000")
						.estimatedTotalPremiums("25000").lumpSumPremiums("125000").build())
				.enforcedPolicyInfo(null).currencyPay("JPY").source("ING").build();

		set.add(one);
		set.add(two);
		set.add(three);
		set.add(four);
		set.add(five);
		return set;

	}

	Function<Set<Common>, Mono<ElderlyCheckResponse>> generateElderlyCheckResponse(ElderlyCheckRequest request,
			Set<ApplicationInfo> applicationInfoSet, Set<Common> common) {
		Map<PolicyTypeConstant, Set<Common>> partitionedCommon = common.parallelStream().unordered()
				.collect(Collectors.groupingByConcurrent(Common::getPolicyTypeConstant,
						Collectors.toCollection(CopyOnWriteArraySet::new)));
		BigDecimal lumpSumPremium_Inforce = calculateLumpSumPremiumForInforce(partitionedCommon);
		BigDecimal levelPremium_Inforce = calculateLevelPremiumForInforce(partitionedCommon);
		BigDecimal lumpSumPremium_Pending = calculateLumpSumPremiumForPending(partitionedCommon);
		BigDecimal levelPremium_Pending = calculateLevelPremiumForPending(partitionedCommon);
		System.out.println("lumpSumPremium_Inforce: " + lumpSumPremium_Inforce);
		System.out.println("levelPremium_Inforce: " + levelPremium_Inforce);
		System.out.println("lumpSumPremium_Pending: " + lumpSumPremium_Pending);
		System.out.println("levelPremium_Pending: " + levelPremium_Pending);

		return null;

		// generateResponseForLumpSumPremium();
		// generateResponseForLevelPremium();
	}

	private BigDecimal calculateLumpSumPremiumForInforce(Map<PolicyTypeConstant, Set<Common>> partitionedCommon) {
		Function<Common, BigDecimal> enforcedLumpSum_totalPremium = common -> new BigDecimal(
				common.getCurrencyPay().equals("JPY") ? common.getEnforcedPolicyInfo().getLumpSumPremiums() : "150");
		return calculateExistingPolicyParameters(partitionedCommon, PolicyTypeConstant.ENFORCED,
				PaymentModeConstant.LUMP_SUM, enforcedLumpSum_totalPremium);

	}

	private BigDecimal calculateLumpSumPremiumForPending(Map<PolicyTypeConstant, Set<Common>> partitionedCommon) {
		Function<Common, BigDecimal> enforcedLumpSum_totalPremium = common -> new BigDecimal(
				common.getCurrencyPay().equals("JPY") ? common.getPendingPolicyInfo().getLumpSumPremiums() : "150");
		return calculateExistingPolicyParameters(partitionedCommon, PolicyTypeConstant.PENDING,
				PaymentModeConstant.LUMP_SUM, enforcedLumpSum_totalPremium);

	}

	private BigDecimal calculateLevelPremiumForInforce(Map<PolicyTypeConstant, Set<Common>> partitionedCommon) {
		Function<Common, BigDecimal> enforcedLumpSum_totalPremium = common -> null != common.getEnforcedPolicyInfo()
				.getTotalAnnualPremiums()
						? rateMasterUtil.getRateMasterConverstionRate(common,
								new BigDecimal(common.getEnforcedPolicyInfo().getTotalAnnualPremiums()))
						: BigDecimal.ZERO;
		return calculateExistingPolicyParameters(partitionedCommon, PolicyTypeConstant.ENFORCED,
				PaymentModeConstant.LEVEL, enforcedLumpSum_totalPremium);

	}

	private BigDecimal calculateLevelPremiumForPending(Map<PolicyTypeConstant, Set<Common>> partitionedCommon) {
		Function<Common, BigDecimal> enforcedLumpSum_totalPremium = common -> new BigDecimal(
				common.getCurrencyPay().equals("JPY") ? common.getPendingPolicyInfo().getTotalAnnualPremiums()
						: "1500");
		return calculateExistingPolicyParameters(partitionedCommon, PolicyTypeConstant.PENDING,
				PaymentModeConstant.LEVEL, enforcedLumpSum_totalPremium);

	}

	private BigDecimal calculateExistingPolicyParameters(Map<PolicyTypeConstant, Set<Common>> partitionedCommon,
			PolicyTypeConstant pendingOrEnforced, PaymentModeConstant paymentMode,
			Function<Common, BigDecimal> parameterFromSet) {
		return Optional.ofNullable(partitionedCommon.get(pendingOrEnforced))
				.map(commonSet -> getRequiredSet(commonSet, pendingOrEnforced, paymentMode, parameterFromSet).stream()
						.map(parameterFromSet).reduce(BigDecimal.ZERO, BigDecimal::add))
				.orElse(BigDecimal.ZERO);

	}

	private Set<Common> getRequiredSet(Set<Common> commonSet, PolicyTypeConstant pendingOrEnforced,
			PaymentModeConstant paymentModeConstant, Function<Common, BigDecimal> parameterFromSet) {
		Set<Common> specificSet = commonSet.stream()
				.filter(common -> common.getPaymentModeConstant().equals(paymentModeConstant))
				.collect(Collectors.toSet());
		return pendingOrEnforced.equals(PolicyTypeConstant.ENFORCED) ? specificSet : specificSet;
	}

}
