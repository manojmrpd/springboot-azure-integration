package com.spring.reactive.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.reactive.web.dto.HistoryCheckRequest;
import com.spring.reactive.web.dto.HistoryCheckResponse;
import com.spring.reactive.web.service.HistoryCheckService;

import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1")
@Log4j2
public class HistoryCheckController {
	
	@Autowired
	private HistoryCheckService historyCheckService;

	@PostMapping(value = "/history-check", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Mono<HistoryCheckResponse> historyCheck(@RequestBody Mono<HistoryCheckRequest> historyCheckRequest) {
		return historyCheckService.historyCheck(historyCheckRequest);
	}

}
