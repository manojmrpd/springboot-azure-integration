package com.spring.reactive.web.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrentApplicationInfo {
	private LocalDate baseDate;
	private BigDecimal totalAnnualPremiums;
	private BigDecimal estimatedTotalPremiums;
	private BigDecimal lumpSumPremiums;
	private Integer paymentMethod;

}
