package com.spring.reactive.web.dto;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ElderlyCheckRequest {
	@NotBlank
	private String nameKana;
	private Integer gender;
	private String dob;
	private BigDecimal annualIncome;
	private BigDecimal financialAssets;
	private Set<ApplicationInfo> applicationInfo;
	private boolean isFromApollo = false;

}
