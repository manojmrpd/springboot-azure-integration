package com.spring.reactive.web.repository;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.spring.reactive.web.document.PendingPolicyDocument;

import reactor.core.publisher.Flux;

@Repository
public interface PendingPolicyRepository extends ReactiveMongoRepository<PendingPolicyDocument, String> {
    Flux<PendingPolicyDocument> findByDataSourceIn(Set<String> sources);
    Flux<PendingPolicyDocument> findByApplicationNumberIn(Set<Long> applicationNumber);
}
