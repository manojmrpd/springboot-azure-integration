package com.spring.reactive.web.validation.document;

import com.spring.reactive.web.constant.PendingPolicyConstant;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
@AllArgsConstructor
public class ConstraintViolationUtil {

    private final Validator validator;

    public <T> Publisher<T> validate(final T object) {
        final Set<ConstraintViolation<T>> violations =
                new HashSet<>(validator.validate(object));
        if (violations.isEmpty()) {
            return Mono.just(object);
        } else {
            final String message =
                    violations.parallelStream().unordered().map(Object::toString).collect(Collectors.joining(PendingPolicyConstant.COMMA));
            log.error(PendingPolicyConstant.ERROR_VALIDATE, object, message);
            return Mono.empty();
        }
    }
}
