package com.spring.reactive.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public final class Deposit {
    @JsonProperty("productFamilyCodeStandard1")
    private Integer productFamilyCodeStandard1;
    @JsonProperty("policyNumber")
    private Integer policyNumber;
    @JsonProperty("soJournalDate")
    private String soJournalDate;
    @JsonProperty("depositHistorySequenceNumber")
    private Integer depositHistorySequenceNumber;
    @JsonProperty("premiumDueMonth")
    private String premiumDueMonth;
    @JsonProperty("payedAmount")
    private String payedAmount;
    @JsonProperty("tradOurnaldate")
    private String tradOurnaldate;
    @JsonProperty("receivedDate")
    private String receivedDate;
    @JsonProperty("numberOfDepositsRefunds")
    private Integer numberOfDepositsRefunds;
    @JsonProperty("collectionMethodCode")
    private String collectionMethodCode;
    @JsonProperty("collectionCategoryCode")
    private String collectionCategoryCode;
    @JsonProperty("currencyExchangeEffectiveDate")
    private String currencyExchangeEffectiveDate;
    @JsonProperty("currencyCode")
    private String currencyCode;
}