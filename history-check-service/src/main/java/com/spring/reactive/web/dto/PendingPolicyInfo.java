package com.spring.reactive.web.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PendingPolicyInfo {
	private String premiumMode;
	private String totalAnnualPremiums;
	private String estimatedTotalPremiums;
	private String lumpSumPremiums;

}
