package com.spring.reactive.web.opencsv;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
public final class VantagePendingPolicy extends PendingPolicy {
    @CsvBindByPosition(position = 23)
    @NotBlank
    @Size(max = 20)
    private String prefecture;
    @CsvBindByPosition(position = 24)
    @NotBlank
    @Size(max = 30)
    private String cityName;
    @CsvBindByPosition(position = 25)
    @NotBlank
    @Size(max = 50)
    private String addr1;
}
