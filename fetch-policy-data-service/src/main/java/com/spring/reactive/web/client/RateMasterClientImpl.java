package com.spring.reactive.web.client;

import com.spring.reactive.web.client.dto.RateMasterExchangeResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
@Service
@Slf4j
public class RateMasterClientImpl implements RateMasterClient {
    @Override
    public Flux<RateMasterExchangeResponse> exchangeRates(final LocalDate issueDate) {
        return null;
    }
}
