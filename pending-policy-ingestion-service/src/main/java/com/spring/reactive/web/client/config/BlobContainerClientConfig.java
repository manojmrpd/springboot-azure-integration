package com.spring.reactive.web.client.config;

import com.azure.storage.blob.BlobContainerAsyncClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.spring.reactive.web.config.property.ConfigurationPropertiesConfig;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class BlobContainerClientConfig {

    private final ConfigurationPropertiesConfig configurationPropertiesConfig;

    @Bean
    BlobContainerAsyncClient blobContainerAsyncClient() {
        return new BlobServiceClientBuilder().connectionString(configurationPropertiesConfig.getBlobConnectionString())
                .buildAsyncClient()
                .getBlobContainerAsyncClient(configurationPropertiesConfig.getBlobContainerName());
    }
}
