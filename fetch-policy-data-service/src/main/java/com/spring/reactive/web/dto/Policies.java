package com.spring.reactive.web.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder(toBuilder = true)
public class Policies {
    private String policyNumber;
    private PolicyDetails policyDetails;
}
