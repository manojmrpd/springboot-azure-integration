package com.spring.reactive.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static com.spring.reactive.web.constant.PendingPolicyConstant.COM_MANULIFE_AP_VALIDATION_DATE_DATE_VALIDATOR_MESSAGE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = DateValidatorImpl.class)
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface DateValidator {
    String message() default COM_MANULIFE_AP_VALIDATION_DATE_DATE_VALIDATOR_MESSAGE;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
