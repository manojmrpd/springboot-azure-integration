package com.spring.reactive.web.service.util;

import com.spring.reactive.web.client.RateMasterClient;
import com.spring.reactive.web.client.dto.PolicyInfo;
import com.spring.reactive.web.client.dto.PolicyResponse;
import com.spring.reactive.web.client.dto.RateMasterExchangeResponse;
import com.spring.reactive.web.constant.HistoryCheckConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;

@Component
public class RaterMasterServiceUtil {
    @Autowired
    private RateMasterClient rateMasterClient;

    public Flux<PolicyResponse> getExchangeRates(PolicyResponse policyResponse, String levelOrLumpSum, String currencyPay, String applicationDate, boolean totalAnnualPremiums) {
        LocalDate effectiveDateIn = LocalDate.parse(applicationDate);
        return rateMasterClient.exchangeRates(effectiveDateIn.isAfter(LocalDate.now()) ? LocalDate.now() : effectiveDateIn).flatMap(rateMasterExchangeResponse -> {
            if (totalAnnualPremiums) {
                policyResponse.getPolicy().setTotalAnnualPremiums(convertToJapanCurrency(rateMasterExchangeResponse, currencyPay,
                        new BigDecimal(levelOrLumpSum), false).toString());
                return Flux.just(policyResponse);
            } else {
                final String lumpSumPremium = policyResponse.getPolicy().getLumpSumPremium();
                policyResponse.getPolicy().setLumpSumPremium(Optional.ofNullable(lumpSumPremium).isPresent() ?
                        new BigDecimal(lumpSumPremium).add(convertToJapanCurrency(rateMasterExchangeResponse,
                                currencyPay, new BigDecimal(levelOrLumpSum), true)).toString() :
                        convertToJapanCurrency(rateMasterExchangeResponse, currencyPay,
                                new BigDecimal(levelOrLumpSum), false).toString());
                return Flux.just(policyResponse);
            }
        });
    }

    private BigDecimal convertToJapanCurrency(RateMasterExchangeResponse rateMasterExchangeResponse, String currencyPay, BigDecimal levelOrLumpSum, boolean lumpSum) {
        var value = levelOrLumpSum.multiply(getForexRates(currencyPay,
                rateMasterExchangeResponse));
        return lumpSum ? value.setScale(HistoryCheckConstant.ZERO, RoundingMode.UP) :
                value.setScale(HistoryCheckConstant.ZERO, RoundingMode.HALF_UP);
    }

    private BigDecimal getForexRates(String currencyPay, RateMasterExchangeResponse rateMasterExchangeResponse) {
        return new BigDecimal(rateMasterExchangeResponse.getExchangeRates().parallelStream().unordered().filter(exchangeRate ->
                exchangeRate.getBaseCurrency().equals(currencyPay)).findAny().orElseThrow().getExchangeRate().parallelStream().unordered().findAny().orElseThrow().getRate());
    }
}
