package com.spring.reactive.web.client.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;
@Builder
@Data
public class ApplicationInfo {
	private String applicationNumber;
	private String paymentMethod;
	private BigDecimal totalAnnualPremium;
	private BigDecimal lumpSumPremium;
	private LocalDate applicationDate;
	

}
