package com.spring.reactive.web.service.dto;

import java.util.List;

public class PolicyInfo {
	
	private String lumpSumPremium;
	
	private List<FirstPremiumInfo> firstPremiumInfo;

	public String getLumpSumPremium() {
		return lumpSumPremium;
	}

	public void setLumpSumPremium(String lumpSumPremium) {
		this.lumpSumPremium = lumpSumPremium;
	}

	public List<FirstPremiumInfo> getFirstPremiumInfo() {
		return firstPremiumInfo;
	}

	public void setFirstPremiumInfo(List<FirstPremiumInfo> firstPremiumInfo) {
		this.firstPremiumInfo = firstPremiumInfo;
	}

	@Override
	public String toString() {
		return "PolicyInfo [lumpSumPremium=" + lumpSumPremium + ", firstPremiumInfo=" + firstPremiumInfo + "]";
	}

	public PolicyInfo(String lumpSumPremium, List<FirstPremiumInfo> firstPremiumInfo) {
		super();
		this.lumpSumPremium = lumpSumPremium;
		this.firstPremiumInfo = firstPremiumInfo;
	}

	public PolicyInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	

}
