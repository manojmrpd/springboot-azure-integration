package com.spring.reactive.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

@Builder
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class PolicyInfo {

    private Integer productFamilyCodeStandard1;
    private Integer productFamilyCodeStandard2;
    private Integer policyNumber;
    private String restrictBillingCode;
    @NotBlank
    private String issueDate;
    private String policyStatus;
    private String paymentMethod;
    @NotBlank
    private String premiumMode;
    private String premiumAmount;
    private String isAssigneeFlag;
    private String totalFaceAmount;
    private Integer agentCode;
    private String scheduledLumpSumPayment;
    private String prePaymentAmount;
    private String premiumPaymentBalance;
    private String premiumPaymentCompletionFlag;
    private Integer regionalOffice;
    private Integer salesOffice;
    private String targetAmount;
    private Integer targetRate;
    private String planCode;
    private String premiumPayPeriod;
    private String totalAnnualPremiums;
    private String estimatedTotalPremiums;
    private String currencyPay;
    private String contractCurrencyCode;
    private String levelPremium;
    private String lumpSumPremium;
    private String policyIssueDateBeforeChange;
    private String paidToDate;
    private String prodTypeCode;
    private String advancePaymentStartDate;
    private String tradPaidDate;
    private List<PolicyClientInfo> policyClients;
    private List<CategoryInfo> categories;
    private List<Fund> funds;
    private Set<FirstPremiumInfo> firstPremiumInfo;
    private Set<Coverage> coverage;
    private Set<Deposit> deposits;
    private Set<CnvrtdDeathInsrncDetails> cnvrtdDeathInsrncDetails;

}
