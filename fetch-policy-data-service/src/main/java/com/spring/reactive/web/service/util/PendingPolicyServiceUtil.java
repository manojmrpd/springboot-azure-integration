package com.spring.reactive.web.service.util;

import java.util.Optional;
import java.util.function.Function;

import com.spring.reactive.web.dto.HistoryCheckRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spring.reactive.web.config.ConfigurationPropertiesConfig;
import com.spring.reactive.web.constant.HistoryCheckConstant;
import com.spring.reactive.web.constant.PaymentModeConstant;
import com.spring.reactive.web.constant.PolicyTypeConstant;
import com.spring.reactive.web.dto.Common;
import com.spring.reactive.web.dto.ElderlyCheckRequest;
import com.spring.reactive.web.dto.PendingPolicyInfo;
import com.spring.reactive.web.repository.PendingPolicyRepository;

import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Flux;

@Component
@Log4j2
public class PendingPolicyServiceUtil {

	@Autowired
	private PendingPolicyRepository pendingPolicyRepository;

	@Autowired
	private ConfigurationPropertiesConfig configurationPropertiesConfig;

	public Flux<Common> getPendingPolicies(final HistoryCheckRequest historyCheckRequest) {
		return pendingPolicyRepository.getElderlyPendingPolicies(historyCheckRequest.getElderlyCheckRequest())
				.filter(pendingPolicyDocument -> historyCheckRequest.getElderlyCheckRequest().getApplicationInfo().stream().filter(
						applicationInfo -> Optional.ofNullable(applicationInfo.getApplicationNumber()).isPresent())
						.noneMatch(applicationInfo -> applicationInfo.getApplicationNumber()
								.equals(pendingPolicyDocument.getApplicationNumber().toString())))
				.log()
				.filter(pendingPolicyDocument -> pendingPolicyDocument.getDataSource().equals(HistoryCheckConstant.BPM)
						&& !pendingPolicyDocument.getPremiumMode().equals("99")
								? null != pendingPolicyDocument.getEstimatedTotalPremiumsTtmRate()
								: true)
				.flatMap(pendingPolicyDocument -> {
					Optional.of(pendingPolicyDocument.getDataSource()).filter(dataSource -> dataSource.equals(HistoryCheckConstant.DIS))
							.ifPresent(s -> pendingPolicyDocument.setPremiumMode("8"));
					return Flux.just(Common.builder().policyTypeConstant(PolicyTypeConstant.PENDING)
							.paymentModeConstant(Optional.of(pendingPolicyDocument.getPremiumMode())
									.map(returnPaymentMethod()).orElseThrow())
							.pendingPolicyInfo(PendingPolicyInfo.builder()
									.estimatedTotalPremiums(pendingPolicyDocument.getEstimatedTotalPremiumsTtmRate())
									.premiumMode(pendingPolicyDocument.getPremiumMode())
									.lumpSumPremiums(pendingPolicyDocument.getLumpSumPremiumsTtmRate())
									.totalAnnualPremiums(pendingPolicyDocument.getTotalAnnualPremiumsTtmRate()).build())
							.build());

				}).log().doOnComplete(() -> log.info("Fetching pending policies for elderly limit check"));
	}

	private Function<String, PaymentModeConstant> returnPaymentMethod() {
		return (final String string) -> {
			if (configurationPropertiesConfig.getPaymentMethods().getLevelList().stream()
					.anyMatch(level -> level.equals(Integer.parseInt(string)))) {
				return PaymentModeConstant.LEVEL;
			} else if (configurationPropertiesConfig.getPaymentMethods().getLumpSumList().stream()
					.anyMatch(lumpsum -> lumpsum.equals(Integer.parseInt(string)))) {
				return PaymentModeConstant.LUMP_SUM;
			}
			throw new RuntimeException("Invalid premium mode");
		};
	}

}
