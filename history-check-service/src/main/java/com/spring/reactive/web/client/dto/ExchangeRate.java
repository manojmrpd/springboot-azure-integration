package com.spring.reactive.web.client.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Set;
@Data
@Builder
@ToString
public class ExchangeRate {

    private String baseCurrency;
    private Set<InnerExchangeRate> exchangeRate;
}
