package com.spring.reactive.web.client;

import com.spring.reactive.web.client.dto.PolicyResponse;
import com.spring.reactive.web.dto.ElderlyCheckRequest;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public interface PolicyMasterClient {

    public Flux<PolicyResponse> getPoliciesFromPolicyMaster(final ElderlyCheckRequest elderlyCheckRequest);
}
