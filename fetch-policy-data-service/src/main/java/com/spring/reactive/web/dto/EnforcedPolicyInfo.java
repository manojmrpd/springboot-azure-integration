package com.spring.reactive.web.dto;

import com.spring.reactive.web.client.dto.PolicyClientInfo;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Builder(toBuilder = true)
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EnforcedPolicyInfo {
    private String nameKana;
    private String dob;
    private Integer gender;
    private LocalDate baseDate;
    private LocalDate issueDate;
    private String totalAnnualPremiums;
    private String estimatedTotalPremiums;
    private String lumpSumPremiums;
    private String premiumMode;
    private List<PolicyClientInfo> policyClientsList;
}
