package com.spring.reactive.web;

import com.spring.reactive.web.config.property.ConfigurationPropertiesConfig;
import lombok.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Generated
@SpringBootApplication
@EnableConfigurationProperties(ConfigurationPropertiesConfig.class)
public class PendingPolicyIngestionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PendingPolicyIngestionServiceApplication.class, args);
	}

}
