package com.spring.reactive.web.config.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;
import java.util.Set;

@Configuration
@Setter
@Getter
@PropertySource("classpath:application.properties")
@ConfigurationProperties
@ConfigurationPropertiesScan
public class ConfigurationPropertiesConfig {

    @Value("${custom.file.backup}")
    private String backup;

    @Value("${custom.file.local}")
    private String local;

    @Value("${custom.file.ingenium.directory}")
    private String ingeniumDirectory;

    @Value("${custom.file.ingenium.filename}")
    private String ingeniumFileName;
    @Value("${custom.file.vantage.directory}")
    private String vantageDirectory;
    @Value("${custom.file.vantage.filename}")
    private String vantageFileName;

    @Value("${custom.azure.blob.connection-string}")
    private String blobConnectionString;
    @Value("${custom.azure.blob.container-name}")
    private String blobContainerName;

    @Value("${custom.sources}")
    private Set<String> sources;

}
