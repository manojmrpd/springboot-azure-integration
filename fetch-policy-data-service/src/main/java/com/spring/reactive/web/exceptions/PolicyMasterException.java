package com.spring.reactive.web.exceptions;

import org.springframework.web.reactive.function.client.WebClientException;

public class PolicyMasterException extends WebClientException {

    private int httpStatusCode;

    public PolicyMasterException(String msg) {
        super(msg);
    }

    public PolicyMasterException(String msg, int httpStatusCode) {
        super(msg);
        this.httpStatusCode = httpStatusCode;
    }
}
