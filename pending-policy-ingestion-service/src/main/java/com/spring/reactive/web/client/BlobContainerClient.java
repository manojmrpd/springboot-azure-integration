package com.spring.reactive.web.client;

import com.azure.storage.blob.BlobContainerAsyncClient;
import com.azure.storage.blob.models.BlobProperties;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class BlobContainerClient {

    private final BlobContainerAsyncClient blobContainerAsyncClient;

    public Mono<BlobProperties> downloadToFile(String blob, String local) {
        log.info("Downloading the file from azure blob to local directory");
        Optional.ofNullable(Path.of(blob).getParent()).ifPresent(this::createDirectories);
        return blobContainerAsyncClient.getBlobAsyncClient(blob).downloadToFile(local, Boolean.TRUE);
    }

    @SneakyThrows
    private void createDirectories(Path path) {
        Files.createDirectories(path);
    }
}
