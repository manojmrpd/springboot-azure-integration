package com.spring.reactive.web.service.util;

import com.ibm.icu.text.Transliterator;

public class WidthConversionUtil {

    public static String convertFullWidthToHalfWidth(String nameKana) {
        Transliterator transliterator = Transliterator.getInstance("Fullidth-Halfwidth");
        return transliterator.transliterate(nameKana);
    }
}
