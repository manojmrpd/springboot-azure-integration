package com.spring.reactive.web.controller;

import com.spring.reactive.web.controller.dto.PolicyMasterRequest;
import com.spring.reactive.web.controller.dto.PolicyResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.reactive.web.service.EnforcedPolicyService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1")
public class EnforcedPolicyController {

    @Autowired
    private EnforcedPolicyService enforcedPolicyService;

    @PostMapping(value = "/policies", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<PolicyResponse> searchPoliciesByCustomer(@RequestBody PolicyMasterRequest policyMasterRequest) {
        return enforcedPolicyService.searchPoliciesByCustomer(policyMasterRequest);
    }

    @PostMapping(value = "/ingest")
    public Mono<Void> ingest() {
        return enforcedPolicyService.ingest();
    }
}
