package com.spring.reactive.web.service.util;

import com.spring.reactive.web.config.ConfigurationPropertiesConfig;
import com.spring.reactive.web.constant.PaymentModeConstant;
import com.spring.reactive.web.constant.PolicyTypeConstant;
import com.spring.reactive.web.dto.ApplicationInfo;
import com.spring.reactive.web.dto.Common;
import com.spring.reactive.web.dto.CurrentApplicationInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.reactivestreams.Publisher;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

@Component
public class AppliedPolicyServiceUtil {

    private ConfigurationPropertiesConfig configurationPropertiesConfig;

    public Publisher<? extends Common> getAppliedPolicies(Set<ApplicationInfo> applicationInfoSet) {
        return Flux.fromIterable(applicationInfoSet).
                flatMap(applicationInfo -> Mono.just(Common.builder().policyTypeConstant(PolicyTypeConstant.APPLIED)
                        .paymentModeConstant(Optional.of(applicationInfo.getPaymentMethod()).map(Object::toString)
                                .map(returnPaymentMethod()).orElseThrow())
                        .currentApplicationInfo(CurrentApplicationInfo.builder()
                                .baseDate(LocalDate.parse(applicationInfo.getBaseDate()))
                                .paymentMethod(applicationInfo.getPaymentMethod())
                                .estimatedTotalPremiums(getEstimatedTotalPremiumForAppliedPolicies(applicationInfo))
                                .lumpSumPremiums(applicationInfo.getLumpSumPremium())
                                .totalAnnualPremiums(applicationInfo.getAnnualPremium())
                                .build())
                        .build()));
    }

    private BigDecimal getEstimatedTotalPremiumForAppliedPolicies(ApplicationInfo applicationInfo) {
        if (returnPaymentMethod().apply(applicationInfo.getPaymentMethod().toString()).equals(PaymentModeConstant.LEVEL) &&
                ObjectUtils.isNotEmpty(applicationInfo.getEstimatedTotalPremium())) {
            return applicationInfo.getEstimatedTotalPremium();
        } else {
            return BigDecimal.ZERO;
        }
    }

    private Function<String, PaymentModeConstant> returnPaymentMethod() {
        return (final String paymentMethod) -> {
            if (configurationPropertiesConfig.getPaymentMethods().getLevelList().stream()
                    .anyMatch(level -> level.equals(Integer.parseInt(paymentMethod)))) {
                return PaymentModeConstant.LEVEL;
            } else if (configurationPropertiesConfig.getPaymentMethods().getLumpSumList().stream()
                    .anyMatch(lumpsum -> lumpsum.equals(Integer.parseInt(paymentMethod)))) {
                return PaymentModeConstant.LUMP_SUM;
            }
            throw new RuntimeException("Invalid premium mode");
        };
    }
}
