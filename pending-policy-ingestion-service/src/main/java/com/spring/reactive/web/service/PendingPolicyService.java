package com.spring.reactive.web.service;

import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;
@Service
public interface PendingPolicyService {

	Mono<Void> getPendingPolicies();

}
