/*
package com.spring.reactive.web.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.spring.reactive.web.client.HistoryCheckWebClient;
import com.spring.reactive.web.client.dto.ElderlyCheckResponse;
import com.spring.reactive.web.client.dto.HistoryCheckResponse;
import com.spring.reactive.web.document.PendingPolicyDocument;
import com.spring.reactive.web.document.PendingPolicyClientDocument;
import com.spring.reactive.web.repository.PendingPolicyRepository;
import com.spring.reactive.web.service.impl.PendingPolicyServiceImpl;
import com.spring.reactive.web.service.util.MongoTemplateUtil;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@ActiveProfiles(profiles = "test")
@EnableAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)
public class PendingPolicyServiceTest {

	@MockBean
	private HistoryCheckWebClient webClient;

	@MockBean
	private PendingPolicyRepository pendingPolicyRepository;

	@MockBean
	private MongoTemplateUtil mongoTemplate;

	@Autowired
	private PendingPolicyServiceImpl pendingPolicyServiceImpl;

	//@Test
	void getPendingPoliciesForIng() {
		when(mongoTemplate.getIngPendingPolicyDocument()).thenReturn(Flux.just(getPendingPolicyDocument()));
		when(webClient.invokeHistoryCheck(any())).thenReturn(Mono.just(getHistoryCheckResponse()));
		StepVerifier.create(pendingPolicyServiceImpl.getPendingPolicies()).expectNextCount(1).verifyComplete();

	}

	private HistoryCheckResponse getHistoryCheckResponse() {
		HistoryCheckResponse response = new HistoryCheckResponse();
		response.setElderlyCheckResponse(getElderCheckResponse());
		return null;
	}

	private ElderlyCheckResponse getElderCheckResponse() {
		return null;
	}

	private PendingPolicyDocument getPendingPolicyDocument() {
		Set<PendingPolicyClientDocument> user = new HashSet<>();
		user.add(PendingPolicyClientDocument.builder().nameKana("manoj").nameKana("kumar").birthDate("1953-03-15").gender("male")
				.addressCode("hyderabad").build());
		return PendingPolicyDocument.builder().productFamilyCodeStandard2(500).productFamilyCodeStandard1(100).lumpSumPremiumsTtmRate(
				"5000")
				.effectiveDate(LocalDate.now()).cancellationDate(null).dataSource("ING").totalAnnualPremiumsTtmRate(
						"1000")
				.applicationDate("2023-06-20").policyHolderFinancialAssets("10000").id(545454l).premiumMode("credit " +
						"card")
				.premiumMode("annual").policyNumber(213054).totalAnnualPremiumsTtmRate("1000").updateDate(LocalDate.now())
				.planCode("self insurance").pendingPolicyClients(user).build();
	}
}
*/
