package com.spring.reactive.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class HistoryCheckRequest {
	@Valid
	@NotNull
	private ElderlyCheckRequest elderlyCheckRequest;

}
