package com.spring.reactive.web.client;

import com.spring.reactive.web.client.dto.RateMasterExchangeResponse;
import com.spring.reactive.web.dto.Common;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.LocalDate;

@Service
public interface RateMasterClient {
    Flux<RateMasterExchangeResponse> exchangeRates(LocalDate issueDate);
}
