package com.spring.reactive.web.validation;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static com.spring.reactive.web.constant.PendingPolicyConstant.YYYYMMDD;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Optional.ofNullable;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
@Slf4j
final class DateValidatorImpl implements ConstraintValidator<DateValidator, String> {
    @Override
    public boolean isValid(final String dateFromFile, final ConstraintValidatorContext constraintValidatorContext) {
        var result = TRUE;
        try {
            ofNullable(dateFromFile).ifPresent((final String string) -> log.debug(LocalDate.parse(dateFromFile, ofPattern(YYYYMMDD)).toString()));
        } catch (final DateTimeParseException dateTimeParseException) {
            result = FALSE;
        }
        return result;
    }
}