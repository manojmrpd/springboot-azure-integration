package com.spring.reactive.web.client.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class HistoryCheckRequest {
	private ElderlyCheckRequest elderlyCheckRequest;

}
