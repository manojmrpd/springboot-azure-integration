package com.spring.reactive.web.dto;

import com.spring.reactive.web.constant.PaymentModeConstant;
import com.spring.reactive.web.constant.PolicyTypeConstant;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Common {
    private PolicyTypeConstant policyTypeConstant;
    private PaymentModeConstant paymentModeConstant;
    private PendingPolicyInfo pendingPolicyInfo;
    private CurrentApplicationInfo currentApplicationInfo;
    private EnforcedPolicyInfo enforcedPolicyInfo;
    private boolean existingPolicy;
    private boolean fullyAdvancedPremium;
    private boolean policyForPayment;

}
