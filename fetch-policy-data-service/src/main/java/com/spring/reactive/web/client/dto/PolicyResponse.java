package com.spring.reactive.web.client.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Builder(toBuilder = true)
@ToString
public class PolicyResponse {

    private String id;

    private PolicyInfo policy;

    private String status;

    private String statusDesc;
}
