package com.spring.reactive.web.service.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PendingPolicyInfo {
	private String premiumMode;
	private String totalAnnualPremiums;
	private String estimatedTotalPremiums;
	private String lumpSumPremiums;

}
