package com.spring.reactive.web.service.impl;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.spring.reactive.web.client.HistoryCheckWebClient;
import com.spring.reactive.web.client.dto.*;
import com.spring.reactive.web.constant.PendingPolicyConstant;
import com.spring.reactive.web.document.PendingPolicyDocument;
import com.spring.reactive.web.opencsv.IngeniumPolicyResponse;
import com.spring.reactive.web.opencsv.VantagePolicyResponse;
import com.spring.reactive.web.service.util.MongoTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class BatchProcessServiceImpl {

    @Autowired
    private HistoryCheckWebClient webClient;

    @Autowired
    private MongoTemplateUtil mongoTemplate;

    public Disposable processIngenium() {
        return mongoTemplate.getIngPendingPolicyDocument().log().filter(this::calculateAge).log()
                .flatMap(this::invokeHistoryCheck).flatMap(this::getIngeniumCsvBean).log().collectList()
                .flatMap(this::writeCsvToIng).subscribe();
    }

    public Disposable processVantage() {
        return mongoTemplate.getVanPendingPolicyDocument().log().filter(this::calculateAge).log()
                .flatMap(this::invokeHistoryCheck).flatMap(this::getVantageCsvBean).collectList().log()
                .flatMap(this::writeCsvToVan).subscribe();
    }

    private Mono<List<IngeniumPolicyResponse>> writeCsvToIng(List<IngeniumPolicyResponse> response) {
        try (Writer writer = Files.newBufferedWriter(Paths.get(PendingPolicyConstant.INGENIUM_RESPONSE_FILE))) {
            ColumnPositionMappingStrategy<IngeniumPolicyResponse> mappingStrategy = new ColumnPositionMappingStrategy<IngeniumPolicyResponse>();
            mappingStrategy.setType(IngeniumPolicyResponse.class);
            StatefulBeanToCsv<IngeniumPolicyResponse> beanToCsv = new StatefulBeanToCsvBuilder<IngeniumPolicyResponse>(
                    writer).withMappingStrategy(mappingStrategy).withSeparator(',')
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();
            beanToCsv.write(response);
        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            log.error("Exception occured fetching the file {}", e.getMessage());
        }
        return Mono.just(response);
    }

    private Mono<List<VantagePolicyResponse>> writeCsvToVan(List<VantagePolicyResponse> response) {
        try (Writer writer = Files.newBufferedWriter(Paths.get(PendingPolicyConstant.VANTAGE_RESPONSE_FILE));) {
            ColumnPositionMappingStrategy<VantagePolicyResponse> mappingStrategy = new ColumnPositionMappingStrategy<VantagePolicyResponse>();
            mappingStrategy.setType(VantagePolicyResponse.class);
            StatefulBeanToCsv<VantagePolicyResponse> beanToCsv = new StatefulBeanToCsvBuilder<VantagePolicyResponse>(
                    writer).withMappingStrategy(mappingStrategy).withSeparator(',')
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();
            beanToCsv.write(response);
        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            log.error("Exception occured fetching the file {}", e.getMessage());
        }
        return Mono.just(response);
    }

    private Flux<IngeniumPolicyResponse> getIngeniumCsvBean(HistoryCheckResponse historyCheckResponse) {
        return Flux.just(getIngeniumResponse(historyCheckResponse.getElderlyCheckResponse()));
    }

    private Flux<VantagePolicyResponse> getVantageCsvBean(HistoryCheckResponse historyCheckResponse) {
        return Flux.just(getVantageResponse(historyCheckResponse.getElderlyCheckResponse()));
    }

    private IngeniumPolicyResponse getIngeniumResponse(ElderlyCheckResponse elderlyCheckResponse) {
        return IngeniumPolicyResponse.builder().largeTypeCode(500)
                .applicationNumber(elderlyCheckResponse.getApplicationNumber())
                .annualIncome(elderlyCheckResponse.getAnnualIncome())
                .financialAssets(elderlyCheckResponse.getFinancialAssets())
                .totalAnnualPremium(elderlyCheckResponse.getTotalAnnualPremium())
                .estimatedTotalPremium(elderlyCheckResponse.getEstimatedTotalPremium())
                .policyCountForOneYear(elderlyCheckResponse.getPolicyCountForOneYear())
                .policyCountForTwoYear(elderlyCheckResponse.getPolicyCountForTwoYear())
                .policyCountForLevelPayment(elderlyCheckResponse.getPolicyCountForLevelPayment())
                .excessPolicyCountForOneYear(elderlyCheckResponse.getExcessPolicyCountForOneYear())
                .excessPolicyCountForTwoYear(elderlyCheckResponse.getExcessPolicyCountForTwoYear())
                .excessPolicyCountForLevelPayment(elderlyCheckResponse.getExcessPolicyCountForLevelPayment())
                .resultCode1(elderlyCheckResponse.getCheckResult().get(0).getResultCode())
                .errorMessage1(elderlyCheckResponse.getCheckResult().get(0).getErrorMessage())
                .resultCode2(elderlyCheckResponse.getCheckResult().get(1).getResultCode())
                .errorMessage2(elderlyCheckResponse.getCheckResult().get(1).getErrorMessage())
                .resultCode3(elderlyCheckResponse.getCheckResult().get(2).getResultCode())
                .errorMessage3(elderlyCheckResponse.getCheckResult().get(2).getErrorMessage())
                .systemErrorCode(elderlyCheckResponse.getSystemErrorCode())
                .systemErrorMessage(elderlyCheckResponse.getSystemErrorMessage()).build();
    }

    private VantagePolicyResponse getVantageResponse(ElderlyCheckResponse elderlyCheckResponse) {
        return VantagePolicyResponse.builder().largeTypeCode(501)
                .applicationNumber(elderlyCheckResponse.getApplicationNumber())
                .annualIncome(elderlyCheckResponse.getAnnualIncome())
                .financialAssets(elderlyCheckResponse.getFinancialAssets())
                .policyCountForOneYear(elderlyCheckResponse.getPolicyCountForOneYear())
                .policyCountForTwoYear(elderlyCheckResponse.getPolicyCountForTwoYear())
                .excessPolicyCountForOneYear(elderlyCheckResponse.getExcessPolicyCountForOneYear())
                .excessPolicyCountForTwoYear(elderlyCheckResponse.getExcessPolicyCountForTwoYear())
                .resultCode1(elderlyCheckResponse.getCheckResult().get(0).getResultCode())
                .errorMessage1(elderlyCheckResponse.getCheckResult().get(0).getErrorMessage())
                .resultCode2(elderlyCheckResponse.getCheckResult().get(1).getResultCode())
                .errorMessage2(elderlyCheckResponse.getCheckResult().get(1).getErrorMessage())
                .resultCode3(elderlyCheckResponse.getCheckResult().get(2).getResultCode())
                .errorMessage3(elderlyCheckResponse.getCheckResult().get(2).getErrorMessage())
                .systemErrorCode(elderlyCheckResponse.getSystemErrorCode())
                .systemErrorMessage(elderlyCheckResponse.getSystemErrorMessage()).build();
    }

    private boolean calculateAge(PendingPolicyDocument policy) {
        if (null != policy && null != policy.getPendingPolicyClients() && !policy.getPendingPolicyClients().isEmpty()
                && null != policy.getApplicationDate()) {
            return Period
                    .between(LocalDate.parse(policy.getPendingPolicyClients().stream().findFirst().get().getDateOfBirth()),
                            LocalDate.parse(policy.getApplicationDate()))
                    .getYears() >= 70;
        }
        return false;

    }

    private Mono<HistoryCheckResponse> invokeHistoryCheck(PendingPolicyDocument pendingPolicyDocument) {
        HistoryCheckRequest request = HistoryCheckRequest.builder()
                .elderlyCheckRequest(ElderlyCheckRequest.builder()
                        .nameKana(pendingPolicyDocument.getPendingPolicyClients().stream().findFirst().get().getNameKana())
                        .gender(pendingPolicyDocument.getPendingPolicyClients().stream().findFirst().get().getGender())
                        .annualIncome(new BigDecimal(pendingPolicyDocument.getPolicyHolderAnnualIncome()))
                        .financialAssets(new BigDecimal(pendingPolicyDocument.getPolicyHolderFinancialAssets()))
                        .applicationInfo(buildApplicationInfo(pendingPolicyDocument)).build())
                .build();
        return webClient.invokeHistoryCheck(Mono.just(request));
    }

    private Set<ApplicationInfo> buildApplicationInfo(PendingPolicyDocument pendingPolicyDocument) {
        Set<ApplicationInfo> applicationInfo = new HashSet<>();
        applicationInfo.add(
                ApplicationInfo.builder().applicationNumber(pendingPolicyDocument.getApplicationNumber().toString())
                        .paymentMethod(pendingPolicyDocument.getPremiumMode())
                        .totalAnnualPremium(new BigDecimal(pendingPolicyDocument.getTotalAnnualPremiumsTtmRate()))
                        .lumpSumPremium(new BigDecimal(pendingPolicyDocument.getPolicyHolderFinancialAssets()))
                        .applicationDate(LocalDate.parse(pendingPolicyDocument.getApplicationDate())).build());

        return applicationInfo;
    }

    private void databaseApproaches() {
        mongoTemplate.usingMongoRepository();
        mongoTemplate.usingMongoQueryByCriteria();
        mongoTemplate.usingMongoAggregation();
    }
}
