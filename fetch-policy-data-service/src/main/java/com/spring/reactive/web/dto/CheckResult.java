package com.spring.reactive.web.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CheckResult {
	private Integer resultCode;
	private String errorMessage;

}
