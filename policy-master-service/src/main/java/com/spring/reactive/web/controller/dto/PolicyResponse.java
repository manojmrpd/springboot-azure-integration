package com.spring.reactive.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.spring.reactive.web.document.Policies;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Builder
@ToString
public class PolicyResponse {

    @JsonProperty("id")
    private String id;

    @JsonProperty("policy")
    private Policies policy;

    @JsonProperty("status")
    private String status;

    @JsonProperty("statusDesc")
    private String statusDesc;
}
