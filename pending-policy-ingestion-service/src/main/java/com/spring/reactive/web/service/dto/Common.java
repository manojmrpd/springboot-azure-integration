package com.spring.reactive.web.service.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Builder
@Data
@ToString
public class Common {
	private PolicyTypeConstant policyTypeConstant;
	private PaymentModeConstant paymentModeConstant;
	private SourceConstant sourceConstant;
	private EnforcedPolicyInfo enforcedPolicyInfo;
	private PendingPolicyInfo pendingPolicyInfo;
	private String source;
	private String currencyPay;
	private PolicyInfo policyInfo;

}
