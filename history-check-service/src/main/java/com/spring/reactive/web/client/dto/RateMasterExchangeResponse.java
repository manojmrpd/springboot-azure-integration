package com.spring.reactive.web.client.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;
import java.util.Set;

@Data
@Builder
@ToString
public class RateMasterExchangeResponse {

    private LocalDate effectiveDate;

    private Set<ExchangeRate> exchangeRates;
}
