package com.spring.reactive.web.service.dto;

public class FirstPremiumInfo {
	
	private String currencyPay;
	
	private String firstPremiumAmount;

	public String getCurrencyPay() {
		return currencyPay;
	}

	public void setCurrencyPay(String currencyPay) {
		this.currencyPay = currencyPay;
	}

	public String getFirstPremiumAmount() {
		return firstPremiumAmount;
	}

	public void setFirstPremiumAmount(String firstPremiumAmount) {
		this.firstPremiumAmount = firstPremiumAmount;
	}

	public FirstPremiumInfo(String currencyPay, String firstPremiumAmount) {
		super();
		this.currencyPay = currencyPay;
		this.firstPremiumAmount = firstPremiumAmount;
	}

	@Override
	public String toString() {
		return "FirstPremiumInfo [currencyPay=" + currencyPay + ", firstPremiumAmount=" + firstPremiumAmount + "]";
	}
	
	

}
