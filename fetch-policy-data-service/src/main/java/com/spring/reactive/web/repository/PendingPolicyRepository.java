package com.spring.reactive.web.repository;

import com.spring.reactive.web.constant.HistoryCheckConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.spring.reactive.web.dto.ElderlyCheckRequest;
import com.spring.reactive.web.repository.document.PendingPolicyDocument;

import reactor.core.publisher.Flux;

@Component
public class PendingPolicyRepository {

	@Autowired
	private ReactiveMongoTemplate reactiveMongoTemplate;

	public Flux<PendingPolicyDocument> getElderlyPendingPolicies(ElderlyCheckRequest elderlyCheckRequest) {
		return reactiveMongoTemplate.find(searchPendingPoliciesByCustomerDetails(elderlyCheckRequest),
				PendingPolicyDocument.class, HistoryCheckConstant.PENDING_POLICIES);

	}
 
	private Query searchPendingPoliciesByCustomerDetails(ElderlyCheckRequest elderlyCheckRequest) {
		Query query = new Query();
		query.addCriteria(Criteria.where(HistoryCheckConstant.PEND_POLICY_CLIENTS)
				.elemMatch(Criteria.where("nameKana").is(elderlyCheckRequest.getNameKana()).and(
						"gender")
						.is(elderlyCheckRequest.getGender()).and("datOfBirth").is(elderlyCheckRequest.getDob())))
				.addCriteria(Criteria.where(HistoryCheckConstant.CANCELLATION_DATE).isNull());
		return query;
	}

}
