package com.spring.reactive.web.service.util;

import reactor.core.publisher.Flux;

public class FluxResearchUtil {

	public static void main(String[] args) {

		Flux<Response> flux = Flux.just(new Response("101", "test"));
		Response result;
		result = flux.single().toFuture().join();
		System.out.println(result);
	}
}

class Response {
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Response(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Response [id=" + id + ", name=" + name + "]";
	}

	public Response() {
		super();
		// TODO Auto-generated constructor stub
	}

}
