package com.spring.reactive.web.constant;

public enum PolicyTypeConstant {
	PENDING, ENFORCED, APPLIED

}
