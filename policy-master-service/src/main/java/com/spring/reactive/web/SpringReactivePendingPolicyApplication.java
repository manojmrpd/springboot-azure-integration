package com.spring.reactive.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactivePendingPolicyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactivePendingPolicyApplication.class, args);
	}

}
