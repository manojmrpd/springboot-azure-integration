package com.spring.reactive.web.service.dto;

public enum PolicyTypeConstant {
	PENDING, ENFORCED, APPLIED

}
