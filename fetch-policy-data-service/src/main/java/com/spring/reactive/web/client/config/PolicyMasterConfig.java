package com.spring.reactive.web.client.config;

import com.spring.reactive.web.constant.HistoryCheckConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Component
@ConfigurationProperties(value = HistoryCheckConstant.POLICY_MASTER_DATA)
public class PolicyMasterConfig {

    @NotNull
    private String uri;

    @NotNull
    private Integer retryCount;

    @NotNull
    private Integer retryAfter;
}
