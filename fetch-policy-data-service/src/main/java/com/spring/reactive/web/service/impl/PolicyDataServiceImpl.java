package com.spring.reactive.web.service.impl;

import com.spring.reactive.web.dto.FetchPolicyRequest;
import com.spring.reactive.web.dto.HistoryCheckResponse;
import com.spring.reactive.web.service.PolicyDataService;
import com.spring.reactive.web.service.util.PendingPolicyUtil;
import com.spring.reactive.web.service.util.WidthConversionUtil;
import org.apache.commons.lang3.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class PolicyDataServiceImpl implements PolicyDataService {

    private PendingPolicyUtil pendingPolicyUtil;
    @Override
    public Mono<HistoryCheckResponse> fetchPolicyDetails(Mono<FetchPolicyRequest> fetchPolicyRequest) {
        fetchPolicyRequest.map(policyRequest -> StringUtils.isNotBlank(policyRequest.getNameKana())
        ?
                        policyRequest.toBuilder().nameKana(WidthConversionUtil.convertFullWidthToHalfWidth(policyRequest.getNameKana())).build()
                : policyRequest
        ).log()
                .flatMapMany(policyRequest -> {
                    if (StringUtils.isNotBlank(policyRequest.getNameKana())) {
                        return Flux.empty();
                    }
                    return PendingPolicyUtil.getPendingPoliciesResponse(policyRequest);
                });
        Mono.()
        return null;
    }
}
