package com.spring.reactive.web.exceptions;

public class SystemException extends RuntimeException {

    private int httpStatusCode;

    public SystemException(String msg) {
        super(msg);
    }

    public SystemException(String msg, int httpStatusCode) {
        super(msg);
        this.httpStatusCode = httpStatusCode;
    }
}
