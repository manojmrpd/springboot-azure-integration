package com.spring.reactive.web.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

@Builder(toBuilder = true)
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class PolicyInfo {

    @JsonProperty("productFamilyCodeStandard1")
    private Integer productFamilyCodeStandard1;
    @JsonProperty("productFamilyCodeStandard2")
    private Integer productFamilyCodeStandard2;
    @JsonProperty("policyNumber")
    private Integer policyNumber;
    @JsonProperty("restrictBillingCode")
    private String restrictBillingCode;
    @JsonProperty("issueDate")
    @NotBlank
    private String issueDate;
    @JsonProperty("policyStatus")
    private String policyStatus;
    @JsonProperty("paymentMethod")
    private String paymentMethod;
    @JsonProperty("premiumMode")
    @NotBlank
    private String premiumMode;
    @JsonProperty("premiumAmount")
    private String premiumAmount;
    @JsonProperty("isAssigneeFlag")
    private String isAssigneeFlag;
    @JsonProperty("totalFaceAmount")
    private String totalFaceAmount;
    @JsonProperty("agentCode")
    private Integer agentCode;
    @JsonProperty("scheduledLumpSumPayment")
    private String scheduledLumpSumPayment;
    @JsonProperty("prePaymentAmount")
    private String prePaymentAmount;
    @JsonProperty("premiumPaymentBalance")
    private String premiumPaymentBalance;
    @JsonProperty("premiumPaymentCompletionFlag")
    private String premiumPaymentCompletionFlag;
    @JsonProperty("regionalOffice")
    private Integer regionalOffice;
    @JsonProperty("salesOffice")
    private Integer salesOffice;
    @JsonProperty("targetAmount")
    private String targetAmount;
    @JsonProperty("targetRate")
    private Integer targetRate;
    @JsonProperty("policyClients")
    private List<PolicyClientInfo> policyClients;
    @JsonProperty("categories")
    private List<CategoryInfo> categories;
    @JsonProperty("funds")
    private List<Fund> funds;
    // Apollo additions
    @JsonProperty("planCode")
    private String planCode;
    @JsonProperty("premiumPayPeriod")
    private String premiumPayPeriod;
    @JsonProperty("totalAnnualPremiumsTTMRate")
    private String totalAnnualPremiums;
    @JsonProperty("estimatedTotalPremiumsTTMRate")
    private String estimatedTotalPremiums;
    @JsonProperty("paymentCurrencyCode")
    private String currencyPay;
    @JsonProperty("contractCurrencyCode")
    private String contractCurrencyCode;
    @JsonProperty("levelPremium")
    private String levelPremium;
    @JsonProperty("lumpSumPremium")
    private String lumpSumPremium;
    @JsonProperty("firstPremiumInfo")
    private Set<FirstPremiumInfo> firstPremiumInfo;
    @JsonProperty("policyIssueDateBeforeChange")
    private String policyIssueDateBeforeChange;
    @JsonProperty("paidToDate")
    private String paidToDate;
    @JsonProperty("prodTypeCode")
    private String prodTypeCode;
    @JsonProperty("advancePaymentStartDate")
    private String advancePaymentStartDate;
    @JsonProperty("tradPaidDate")
    private String tradPaidDate;
    @JsonProperty("coverage")
    private Set<Coverage> coverage;
    @JsonProperty("deposits")
    private Set<Deposit> deposits;
    @JsonProperty("cnvrtdDeathInsrncDetails")
    private Set<CnvrtdDeathInsrncDetails> cnvrtdDeathInsrncDetails;
}
