package com.spring.reactive.web.repository.document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PendingPolicyClientDocument {
	
	private Integer clientType;
	private String nameKanji;
	private String nameKana;
	private String nameKanaConverted;
	private String dateOfBirth;
	private Integer gender;
	private String addressCode;
	private String addressDetail;	

}
