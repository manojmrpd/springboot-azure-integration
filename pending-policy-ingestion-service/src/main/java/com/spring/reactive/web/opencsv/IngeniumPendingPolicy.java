package com.spring.reactive.web.opencsv;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
public final class IngeniumPendingPolicy extends PendingPolicy {
    @CsvBindByPosition(position = 8)
    @Size(max = 5)
    private String premiumPayPeriod;
    @CsvBindByPosition(position = 21)
    @NotBlank
    @Size(max = 19)
    private String totalAnnualPremiumsTtmRate;
    @CsvBindByPosition(position = 22)
    @NotBlank
    @Size(max = 19)
    private String estimatedTotalPremiumsTtmRate;
}