package com.spring.reactive.web.service.impl;

import com.spring.reactive.web.controller.dto.PolicyMasterRequest;
import com.spring.reactive.web.controller.dto.PolicyResponse;
import com.spring.reactive.web.document.Policies;
import com.spring.reactive.web.repository.EnforcedPolicyRepository;
import com.spring.reactive.web.service.EnforcedPolicyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class EnforcedPolicyServiceImpl implements EnforcedPolicyService {

    @Autowired
    private EnforcedPolicyRepository enforcedPolicyRepository;

    @Override
    public Flux<PolicyResponse> searchPoliciesByCustomer(final PolicyMasterRequest policyMasterRequest) {
        Flux<Policies> enforcedPolicies =
                enforcedPolicyRepository.searchPoliciesByCustomer(policyMasterRequest).log();

        log.info("Search policies by customer from policy master service");
        return enforcedPolicies.flatMap(policies -> {
            return Flux.just(PolicyResponse.builder().id(policies.getId().toString()).policy(policies)
                    .status("OK")
                    .statusDesc("Policies search v1 completed")
                    .build());
        });

    }

    @Override
    public Mono<Void> ingest() {
        return Mono.fromCallable(this::processing);
    }

    public Void processing() {
        processFirstMessage()
                .doOnComplete(() -> processSecondMessage().doOnComplete(() -> processThirdMessage()).doOnComplete(() -> log.info("JOB COMPLETED")));
        return null;
    }

    private Flux<String> processFirstMessage() {
        String message = "First Message";
        log.info(message);
        return Flux.just(message);
    }

    private Flux<String> processSecondMessage() {
        String message = "Second Message";
        log.info(message);
        return Flux.just(message);
    }


    private Flux<String> processThirdMessage() {
        String message = "Third Message";
        log.info(message);
        return Flux.just(message);
    }
}
