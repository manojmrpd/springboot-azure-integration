package com.spring.reactive.web.service.util;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.spring.reactive.web.document.PendingPolicyDocument;
import com.spring.reactive.web.repository.PendingPolicyRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class MongoTemplateUtil {

	@Autowired
	private ReactiveMongoTemplate mongoTemplate;

	@Autowired
	private PendingPolicyRepository pendingPolicyRepository;

	public Flux<PendingPolicyDocument> getIngPendingPolicyDocument() {
		Query query = new Query(Criteria.where("cancellationDate").isNull())
		.addCriteria(Criteria.where("productFamilyCode1").is(500))
		.addCriteria(Criteria.where("source").is("ING"));
		return this.mongoTemplate.find(query , PendingPolicyDocument.class);
	}
	
	public Flux<PendingPolicyDocument> getVanPendingPolicyDocument() {
		return this.mongoTemplate.find(new Query(Criteria.where("cancellationDate").isNull())
				.addCriteria(Criteria.where("productFamilyCode1").is(501))
				.addCriteria(Criteria.where("source").is("VAN")), PendingPolicyDocument.class);
	}

	public void usingMongoQueryByCriteria() {
		Query query = new Query(Criteria.where("productFamilyCode1").is(500)).addCriteria(new Criteria()
				.orOperator(Criteria.where("cancellationDate").isNull(), Criteria.where("cancellationDate").is("")));
		Flux<PendingPolicyDocument> flux = mongoTemplate.find(query, PendingPolicyDocument.class);
		flux.doOnNext(System.out::println).subscribe();
	}

	public void usingMongoAggregation() {
		MatchOperation matchOperation = getMatchOperation(500, null);
		this.mongoTemplate.aggregate(Aggregation.newAggregation(PendingPolicyDocument.class, matchOperation),
				PendingPolicyDocument.class);
	}

	public MatchOperation getMatchOperation(Integer productFamilyCode1, LocalDate cancellationDate) {
		Criteria cr = Criteria.where("productFamilyCode1").is(productFamilyCode1).orOperator(
				Criteria.where("cancellationDate").isNull(), Criteria.where("cancellationDate").is(cancellationDate));
		return Aggregation.match(cr);
	}

	public void usingMongoRepository() {

		Flux<String> flux = Flux.fromArray(new String[] { "data1", "data2", "data3" });
		flux.doOnNext(System.out::println).subscribe();
		
		Flux<PendingPolicyDocument> pendingPolicies = pendingPolicyRepository.findAll();
		pendingPolicies.doOnNext(System.out::println).subscribe();

		Mono<List<String>> list =
				pendingPolicies.map(m -> m.getPremiumMode()).collect(Collectors.toList());
		list.subscribe(System.out::println);

		Mono<PendingPolicyDocument> pendingPolicyId = pendingPolicyRepository.findById("64968bf30c6edd4a585c1852");
		pendingPolicyId.subscribe(System.out::println);

	}

}
