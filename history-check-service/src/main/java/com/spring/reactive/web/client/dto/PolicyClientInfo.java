package com.spring.reactive.web.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PolicyClientInfo {
    @JsonProperty("coverageNumber")
    private Integer coverageNumber;
    @JsonProperty("clientType")
    private Integer clientType;
    @JsonProperty("sequenceNumber")
    private Integer sequenceNumber;
    @JsonProperty("scidPre")
    private String scidPre;
    @JsonProperty("scidPost")
    private String scidPost;
    @JsonProperty("nameKana")
    private String nameKana;
    @JsonProperty("birthDate")
    private String birthDate;
    @JsonProperty("gender")
    private Integer gender;
    @JsonProperty("addressCode")
    private String addressCode;
    @JsonProperty("policyholderFlag")
    private Integer policyholderFlag;
    @JsonProperty("insuredFlag")
    private Integer insuredFlag;
    @JsonProperty("hostClientId")
    private String hostClientId;
    @JsonProperty("scvPolicyStatus")
    private Integer scvPolicyStatus;
    @JsonProperty("nameKanji")
    private String nameKanji;
    @JsonProperty("nameKanaConverted")
    private String nameKanaConverted;
    @JsonProperty("address")
    private String address;
}