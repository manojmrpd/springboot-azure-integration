package com.spring.reactive.web.service.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnforcedPolicyInfo {
	private String nameKana;
	private String dob;
	private Integer gender;
	private LocalDate baseDate;
	private LocalDate issueDate;
	private String totalAnnualPremiums;
	private String lumpSumPremiums;
	private String premiumMode;

}
