package com.spring.reactive.web.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder(toBuilder = true)
public class PolicyDetails {
    private String policyNumber;
    private String policyStatus;
    private String petName;
    private String policyIssuedDate;
    private String nameKana;
    private String nameKanji;
    private String nameKanaConverted;
    private String address;
    private String insuredName;
    private String insuredNameKanji;
    private String premiumPayPeriod;
    private String annualizedPremium;
    private String estimatedPremiums;
    private String lumpSumPremium;
    private String currencyCode;
    private String ppmRate;
    private boolean policyHolder;
}
