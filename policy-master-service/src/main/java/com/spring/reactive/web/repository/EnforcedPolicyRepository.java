package com.spring.reactive.web.repository;

import com.spring.reactive.web.controller.dto.PolicyMasterRequest;
import com.spring.reactive.web.controller.dto.PolicyResponse;
import com.spring.reactive.web.document.Policies;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;

import java.util.Optional;

@Repository
@Slf4j
public class EnforcedPolicyRepository {

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    public Flux<Policies> searchPoliciesByCustomer(final PolicyMasterRequest policyMasterRequest) {
        Query query = new Query();
        query = searchPolicyByCustomerDetails(policyMasterRequest, query);
        log.info("Customer search {} ", query);
        return reactiveMongoTemplate.find(query, Policies.class);
    }

    private static Query searchPolicyByCustomerDetails(PolicyMasterRequest policyMasterRequest,
                                                       Query query) {
        if (Optional.ofNullable(policyMasterRequest.getCustomer().getPolicyNumber()).isPresent()) {
            query.addCriteria(Criteria.where("policyNumber").is(Integer.parseInt(policyMasterRequest.getCustomer().getPolicyNumber())));
        }
        Criteria criteria = new Criteria();
        if (Optional.ofNullable(policyMasterRequest.getCustomer().getNameKana()).isPresent()) {
            criteria.and("nameKana").is(policyMasterRequest.getCustomer().getNameKana());
        }
        if (Optional.ofNullable(policyMasterRequest.getCustomer().getGender()).isPresent()) {
            criteria.and("gender").is(policyMasterRequest.getCustomer().getGender());
        }
        if (Optional.ofNullable(policyMasterRequest.getCustomer().getBirthDate()).isPresent()) {
            criteria.and("birthDate").is(policyMasterRequest.getCustomer().getBirthDate());
        }
        return query.addCriteria(Criteria.where("policy.policyClients").elemMatch(criteria));
    }
}
