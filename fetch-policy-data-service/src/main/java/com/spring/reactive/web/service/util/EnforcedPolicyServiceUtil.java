package com.spring.reactive.web.service.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import com.spring.reactive.web.client.PolicyMasterClient;
import com.spring.reactive.web.client.RateMasterClient;
import com.spring.reactive.web.client.dto.Deposit;
import com.spring.reactive.web.client.dto.PolicyInfo;
import com.spring.reactive.web.client.dto.PolicyResponse;
import com.spring.reactive.web.client.dto.RateMasterExchangeResponse;
import com.spring.reactive.web.config.ConfigurationPropertiesConfig;
import com.spring.reactive.web.constant.HistoryCheckConstant;
import com.spring.reactive.web.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;

import static com.spring.reactive.web.constant.HistoryCheckConstant.*;

@Slf4j
@Component
public class EnforcedPolicyServiceUtil {

    @Autowired
    private PolicyMasterClient policyMaserClient;
    @Autowired
    private RaterMasterServiceUtil raterMasterService;
    @Autowired
    private ConfigurationPropertiesConfig configurationPropertiesConfig;

    public Flux<Common> getEnforcedPolicies(HistoryCheckRequest historyCheckRequest) {
        log.info("Fetch enforced policies of the customer");
        policyMaserClient.getPoliciesFromPolicyMaster(historyCheckRequest.getElderlyCheckRequest()).log();
                //.flatMap(policyResponse -> getPolicyResponseForExchangeRates(policyResponse));
        return null;
        //.flatMap(policyResponse -> setEnforcedPolicyData(policyResponse, historyCheckRequest));
    }

    private Flux<PolicyResponse> getPolicyResponseForExchangeRates(PolicyResponse policyResponse) {
        if (ObjectUtils.isNotEmpty(policyResponse.getPolicy()) && StringUtils.isNotBlank(policyResponse.getPolicy().getPremiumMode())
                && policyResponse.getPolicy().getPremiumMode().equals("99")
                || policyResponse.getPolicy().getPremiumMode().equals("1")) {
            return calculateLumpSumPremium(policyResponse);
        } else {
            return calculateTotalAnnualPremium(policyResponse);
        }
    }

    private Flux<PolicyResponse> calculateLumpSumPremium(PolicyResponse policyResponse) {
        if (ObjectUtils.isNotEmpty(policyResponse.getPolicy()) &&
                null != policyResponse.getPolicy().getProductFamilyCodeStandard1()
                && policyResponse.getPolicy().getProductFamilyCodeStandard1().equals(500)) {
            return calculateLumpSumPremiumForIngenium(policyResponse);
        } else if (policyResponse.getPolicy().getPremiumMode().equals(LUMPSUM_MODE) || policyResponse.getPolicy().getPremiumMode()
                .equals(HistoryCheckConstant.LUMPSUM_MODE_UNISYS) && Optional.ofNullable(policyResponse.getPolicy().getLumpSumPremium()).isPresent()) {
            return calculateLumpSumPremiumForVantageUnisys(policyResponse);
        } else {
            policyResponse.getPolicy().setLumpSumPremium(DEFAULT_VALUE);
            return Flux.just(policyResponse);
        }
    }

    private Flux<PolicyResponse> calculateLumpSumPremiumForIngenium(PolicyResponse policyResponse) {
        Predicate<Deposit> isSUL = deposit -> checkForSULProduct(policyResponse, deposit);
        if (ObjectUtils.isNotEmpty(policyResponse.getPolicy().getDeposits())
                && policyResponse.getPolicy().getDeposits().stream().anyMatch(isSUL)) {
            return Flux.fromIterable(policyResponse.getPolicy().getDeposits()).filter(isSUL).flatMapSequential(deposit -> {
                String lumpSumPremium = policyResponse.getPolicy().getLumpSumPremium();
                String payedAmount = deposit.getPayedAmount();
                policyResponse.getPolicy().setLumpSumPremium(Optional.ofNullable(lumpSumPremium).isPresent() ?
                        new BigDecimal(lumpSumPremium).add(new BigDecimal(payedAmount)).toString() : payedAmount
                );
                return Flux.just(policyResponse);
            });
        } else if (ObjectUtils.isNotEmpty(policyResponse.getPolicy().getFirstPremiumInfo())) {
            return Flux.fromIterable(policyResponse.getPolicy().getFirstPremiumInfo()).flatMapSequential(firstPremiumInfo -> {
                var firstPremiumAmount = firstPremiumInfo.getFirstPremiumAmount();
                var firstPremiumPaymentCurrencyCode =
                        firstPremiumInfo.getFirstPremiumPaymentCurrencyCode();
                var lumpSumPremium = policyResponse.getPolicy().getLumpSumPremium();
                if (StringUtils.isNotBlank(firstPremiumPaymentCurrencyCode) && firstPremiumPaymentCurrencyCode.equals(JPY)) {
                    policyResponse.getPolicy().setLumpSumPremium(Optional.ofNullable(lumpSumPremium).isPresent()
                            ?
                            new BigDecimal(lumpSumPremium).add(new BigDecimal(firstPremiumAmount)).toString() : firstPremiumAmount);
                    return Flux.just(policyResponse);
                } else {
                    return raterMasterService.getExchangeRates(policyResponse, firstPremiumAmount
                            , firstPremiumPaymentCurrencyCode, firstPremiumInfo.getReceiptDate(),
                            false);
                }
            });
        } else {
            policyResponse.getPolicy().setLumpSumPremium(DEFAULT_VALUE);
            return Flux.just(policyResponse);
        }
    }

    private Flux<PolicyResponse> calculateLumpSumPremiumForVantageUnisys(PolicyResponse policyResponse) {
        if (null != policyResponse.getPolicy().getCurrencyPay() && policyResponse.getPolicy().getCurrencyPay().equals("JPY")) {
            return Flux.just(policyResponse);
        } else {
            String lumpSumPremium = new BigDecimal(policyResponse.getPolicy().getLumpSumPremium()).movePointLeft(HistoryCheckConstant.TWO).toString();
            return raterMasterService.getExchangeRates(policyResponse, lumpSumPremium,
                    policyResponse.getPolicy().getCurrencyPay(),
                    policyResponse.getPolicy().getIssueDate(), false);
        }
    }

    private Flux<PolicyResponse> calculateTotalAnnualPremium(PolicyResponse policyResponse) {
        if (checkForIngeniumLevelPolicies(policyResponse)) {
            if (policyResponse.getPolicy().getCurrencyPay().equals("JPY")) {
                policyResponse.getPolicy().setTotalAnnualPremiums(new BigDecimal(policyResponse.getPolicy().getLevelPremium()).multiply(getLevelPaymentPerYear(policyResponse.getPolicy())).toString());
                return Flux.just(policyResponse);
            } else {
                return raterMasterService.getExchangeRates(policyResponse,
                        policyResponse.getPolicy().getLevelPremium(), policyResponse.getPolicy().getCurrencyPay(),
                        policyResponse.getPolicy().getIssueDate(), true);
            }
        } else {
            return Flux.just(policyResponse);
        }
    }

    private BigDecimal getLevelPaymentPerYear(PolicyInfo policy) {
        return BigDecimal.valueOf(configurationPropertiesConfig.getPremiumModes().parallelStream().unordered().filter(premiumMode -> premiumMode.getMode().equals(policy.getPremiumMode())).findAny().orElseThrow().getPaymentPerYear());
    }

    private Flux<Common> setEnforcedPolicyData(PolicyResponse policyResponse, HistoryCheckRequest historyCheckRequest) {
        return Flux.just(Common.builder().enforcedPolicyInfo(EnforcedPolicyInfo.builder().nameKana(historyCheckRequest.getElderlyCheckRequest().getNameKana()).gender(historyCheckRequest.getElderlyCheckRequest().getGender()).dob(historyCheckRequest.getElderlyCheckRequest().getDob()).baseDate(!historyCheckRequest.getElderlyCheckRequest().isFromApollo() ? LocalDate.parse(historyCheckRequest.getElderlyCheckRequest().getApplicationInfo().iterator().next().getBaseDate()) : LocalDate.now()).issueDate(LocalDate.parse(policyResponse.getPolicy().getIssueDate())).build()).build());
    }

    private boolean checkForSULProduct(PolicyResponse policyResponse, Deposit deposit) {
        log.info("Inside checkForSULProduct method of AnnualOrLumpSumPremiumCalculatorUtil");
        PolicyInfo policy = policyResponse.getPolicy();
        return null != policy.getPolicyIssueDateBeforeChange()
                && deposit.getCollectionMethodCode().equals(SUL_COLLECTION_METHOD_CODE)
                && deposit.getCollectionCategoryCode().equals(SUL_COLLECTION_CATEGORY_CODE)
                && null != deposit.getPayedAmount()
                && new BigDecimal(deposit.getPayedAmount()).compareTo(BigDecimal.ZERO) > ZERO
                && policy.getPolicyNumber().equals(deposit.getPolicyNumber());
    }

    private boolean checkForIngeniumLevelPolicies(PolicyResponse policyResponse) {
        return null != policyResponse.getPolicy().getProductFamilyCodeStandard1()
                && policyResponse.getPolicy().getProductFamilyCodeStandard1().equals(500)
                && StringUtils.isNotBlank(policyResponse.getPolicy().getLevelPremium())
                && checkExistingLevelPolicies(policyResponse)
                && !checkFullyAdvancedPremiumPolicies(policyResponse);
    }

    private boolean checkExistingLevelPolicies(PolicyResponse policyResponse) {
        log.info("Inside checkExistingLevelPolicies method of AnnualOrLumpSumPremiumCalculatorUtil");
        PolicyInfo policyInfo = policyResponse.getPolicy();
        return configurationPropertiesConfig.getPremiumModes().parallelStream()
                .anyMatch(p -> p.getMode().equals(policyInfo.getPremiumMode()))
                && null == policyInfo.getTotalAnnualPremiums() && null != policyInfo.getPolicyStatus()
                && !(configurationPropertiesConfig.getPolicyStatus().contains(policyInfo.getPolicyStatus()))
                && !(LEVEL_PRODUCT_TYPE_CODE.equals(policyInfo.getProdTypeCode()));
    }

    private boolean checkFullyAdvancedPremiumPolicies(PolicyResponse policyResponse) {
        log.info("check fully advanced premium policies");
        var policyInfo = policyResponse.getPolicy();
        return policyInfo.getPolicyStatus().equals(POLICY_STATUS) && null != policyInfo.getAdvancePaymentStartDate()
                && policyInfo.getAdvancePaymentStartDate().equals(policyInfo.getIssueDate())
                && null != policyInfo.getTradPaidDate()
                && LocalDate.parse(policyInfo.getTradPaidDate()).isAfter(LocalDate.parse(TRAD_PAID_DATE))
                && null != policyInfo.getCoverage()
                && policyInfo.getCoverage().stream()
                .anyMatch(p -> null != p.getRiderCode() && p.getRiderCode().equals(RIDER_CODE))
                && ((policyInfo.getCoverage().stream()
                .anyMatch(p -> p.getMaturityExpiryDate() != null && (LocalDate
                        .parse(policyInfo.getTradPaidDate()).isAfter(LocalDate.parse(p.getMaturityExpiryDate()))
                        || LocalDate.parse(policyInfo.getTradPaidDate())
                        .isEqual(LocalDate.parse(p.getMaturityExpiryDate())))))
                || (policyInfo.getCoverage().stream().anyMatch(p -> p.getUlPaidUpDate() != null && (LocalDate
                .parse(policyInfo.getTradPaidDate()).isAfter(LocalDate.parse(p.getUlPaidUpDate()))
                || LocalDate.parse(policyInfo.getTradPaidDate())
                .isEqual(LocalDate.parse(p.getUlPaidUpDate()))))));
    }
}
