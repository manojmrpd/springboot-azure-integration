package com.spring.reactive.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CnvrtdDeathInsrncDetails {
    @JsonProperty("insNumber")
    private Integer insNumber;
}