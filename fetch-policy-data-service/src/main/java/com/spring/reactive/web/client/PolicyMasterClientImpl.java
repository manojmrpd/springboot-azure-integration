package com.spring.reactive.web.client;

import com.spring.reactive.web.client.config.PolicyMasterConfig;
import com.spring.reactive.web.client.dto.PolicyClientDto;
import com.spring.reactive.web.client.dto.PolicyMasterRequest;
import com.spring.reactive.web.client.dto.PolicyResponse;
import com.spring.reactive.web.constant.HistoryCheckConstant;
import com.spring.reactive.web.dto.ElderlyCheckRequest;
import com.spring.reactive.web.exceptions.PolicyMasterException;
import com.spring.reactive.web.exceptions.SystemException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;

@Component
@Slf4j
@RequiredArgsConstructor
public class PolicyMasterClientImpl implements PolicyMasterClient {
    @Autowired
    private PolicyMasterConfig policyMasterConfig;

    @Autowired
    private WebClient policyMasterWebClient;

    @Override
    public Flux<PolicyResponse> getPoliciesFromPolicyMaster(ElderlyCheckRequest elderlyCheckRequest) {
        PolicyMasterRequest policyMasterRequest = buildPolicyMasterRequest(elderlyCheckRequest);
        return policyMasterWebClient.post().header(HistoryCheckConstant.HEADER_XCLIENTAPP,
                HistoryCheckConstant.HEADER_XCLIENTAPP_VAL)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(policyMasterRequest), PolicyMasterRequest.class)
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("Something went wrong while calling policy master service for client details {} and status is {} ",
                            policyMasterRequest.getCustomer(), clientResponse.rawStatusCode());
                    return Mono.error(new PolicyMasterException("Exception occurred in the policy master service",
                            clientResponse.rawStatusCode()));
                }).onStatus(error -> error.value() == HttpStatus.UNAUTHORIZED.value(),
                        clientResponse -> {
                            log.error("Invalid token provided");
                            return Mono.error(new PolicyMasterException("Invalid token provided", clientResponse.rawStatusCode()));
                        }).onStatus(error -> error.value() == HttpStatus.BAD_REQUEST.value(), clientResponse -> {
                    log.error("Invalid request body provided");
                    return Mono.error(new PolicyMasterException("Invalid request body provided",
                            clientResponse.rawStatusCode()));
                }).onStatus(error -> error.value() == HttpStatus.NOT_FOUND.value(), clientResponse -> {
                    log.info("No policies found from policy master service");
                    return Mono.empty();
                }).bodyToFlux(PolicyResponse.class).log()
                .retryWhen(Retry.backoff(policyMasterConfig.getRetryCount(),
                        Duration.ofMillis(policyMasterConfig.getRetryAfter())));
    }

    private PolicyMasterRequest buildPolicyMasterRequest(ElderlyCheckRequest elderlyCheckRequest) {
       return PolicyMasterRequest.builder().customer(PolicyClientDto.builder().nameKana(elderlyCheckRequest.getNameKana()).
                gender(elderlyCheckRequest.getGender()).birthDate(elderlyCheckRequest.getDob()).build()).build();
    }
}
