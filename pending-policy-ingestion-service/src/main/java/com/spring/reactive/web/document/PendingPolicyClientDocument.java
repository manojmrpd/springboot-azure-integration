package com.spring.reactive.web.document;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Pattern;

import static com.spring.reactive.web.constant.PendingPolicyConstant.DATE_REGEX;

@Builder(toBuilder = true)
@Getter
@Setter
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PendingPolicyClientDocument {
	private Integer clientType;
	private String nameKanji;
	private String nameKana;
	private String nameKanaConverted;
	private String dateOfBirth;
	private Integer gender;
	private String addressCode;
	private String addressDetail;
}
