package com.spring.reactive.web.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PolicyClientDto {

    @JsonProperty("nameKana")
    private String nameKana;
    @JsonProperty("birthDate")
    private String birthDate;
    @JsonProperty("gender")
    private Integer gender;

}
