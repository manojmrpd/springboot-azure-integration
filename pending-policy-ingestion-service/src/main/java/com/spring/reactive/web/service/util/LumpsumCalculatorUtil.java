package com.spring.reactive.web.service.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import com.spring.reactive.web.service.dto.Common;
import com.spring.reactive.web.service.dto.EnforcedPolicyInfo;
import com.spring.reactive.web.service.dto.FirstPremiumInfo;
import com.spring.reactive.web.service.dto.PolicyInfo;

import reactor.core.publisher.Mono;

public class LumpsumCalculatorUtil {

	public static void main(String[] args) {

		EnforcedPolicyInfo enforcedPolicyInfo = new EnforcedPolicyInfo();
		enforcedPolicyInfo.setNameKana("Test");
		enforcedPolicyInfo.setDob("1951-10-01");
		enforcedPolicyInfo.setGender(1);
		enforcedPolicyInfo.setPremiumMode("99");
		enforcedPolicyInfo.setIssueDate(LocalDate.now());
		PolicyInfo policyInfo = new PolicyInfo();
		policyInfo.setLumpSumPremium("1000");
		policyInfo.setFirstPremiumInfo(
				Arrays.asList(new FirstPremiumInfo("JPY", "200"), new FirstPremiumInfo("NZ", "300"),
						new FirstPremiumInfo("JPY", "400"), new FirstPremiumInfo("USD", "200")));
		Common common = Common.builder().policyInfo(policyInfo).enforcedPolicyInfo(enforcedPolicyInfo).build();
		calculateLumpSumPremium(common).subscribe(System.out::println);

	}

	private static Mono<Common> calculateLumpSumPremium(Common common) {
		final var enforcedPolicyInfo = common.getEnforcedPolicyInfo();
		common.getPolicyInfo().getFirstPremiumInfo().forEach(firstPremiumInfo -> {
			final String currencyPay = firstPremiumInfo.getCurrencyPay();
			final var firstPremiumAmount = new BigDecimal(firstPremiumInfo.getFirstPremiumAmount());
			if (currencyPay.equals("JPY")) {
				final String lumpSumPremiums = enforcedPolicyInfo.getLumpSumPremiums();
				enforcedPolicyInfo.setLumpSumPremiums(Optional.ofNullable(lumpSumPremiums).isPresent()
						? new BigDecimal(lumpSumPremiums).add(firstPremiumAmount).toString()
						: firstPremiumAmount.toString());
			} else {
				getForexRate(common, firstPremiumAmount);
			}
		});
		return Mono.just(common);

	}

	private static Mono<Common> getForexRate(final Common common, final BigDecimal firstPremiumAmount) {
		final var enforcedPolicyInfo = common.getEnforcedPolicyInfo();
		final String lumpSumPremiums = enforcedPolicyInfo.getLumpSumPremiums();
		enforcedPolicyInfo.setLumpSumPremiums(Optional.ofNullable(lumpSumPremiums).isPresent()
				? new BigDecimal(lumpSumPremiums).add(convertToJpy(firstPremiumAmount)).toString()
				: convertToJpy(firstPremiumAmount).toString());
		return Mono.just(common);
	}

	private static BigDecimal convertToJpy(BigDecimal lumpSumPremium) {
		return lumpSumPremium.multiply(getPpyRate());
	}

	private static BigDecimal getPpyRate() {
		return new BigDecimal("2");
	}

}
