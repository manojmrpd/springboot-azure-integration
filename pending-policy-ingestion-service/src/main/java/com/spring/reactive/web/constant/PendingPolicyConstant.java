package com.spring.reactive.web.constant;

public class PendingPolicyConstant {
	public static final String HISTORY_CHECK = "/history-check";
	public static final String PENDING_POLICIES = "pending-policies";
	public static final String HISTORY_CHECK_DATA = "history-check-data";

	public  static final String INGENIUM_INPUT_FILE = "E:\\Workflow\\Insurance\\Batch\\inginput" +
			".csv";
	public static final String INGENIUM_RESPONSE_FILE = "E:\\Projects\\Manoj\\spring-reactive-web\\spring-reactive-pending-policy\\src\\main\\resources\\APOLRESING1.csv";
	public static final String VANTAGE_RESPONSE_FILE = "E:\\Projects\\Manoj\\spring-reactive-web\\spring-reactive-pending-policy\\src\\main\\resources\\APOLRESVAN1.csv";
	public static final String CUSTOM = "custom";
	/* Endpoint: */
	public static final String JCUS_API_V_1_INGEST = "/jcus/api/v1/ingest";
	public static final String JCUS_API_V_1_DELETE = "/jcus/api/v1/delete";
	public static final String JCUS = "/jcus/**";
	public static final String ACTUATOR = "/actuator/**";
	/* Number: */
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SIX = 6;
	public static final int SEVEN = 7;
	public static final int EIGHT = 8;
	public static final int NINE = 9;
	public static final int TEN = 10;
	public static final int ELEVEN = 11;
	public static final int TWELVE = 12;
	public static final int THIRTEEN = 13;
	public static final int FOURTEEN = 14;
	public static final int FIFTEEN = 15;
	public static final int SIXTEEN = 16;
	public static final int SEVENTEEN = 17;
	public static final int EIGHTEEN = 18;
	public static final int NINETEEN = 19;
	public static final int TWENTY = 20;
	public static final int TWENTY_ONE = 21;
	public static final int TWENTY_TWO = 22;
	public static final int TWENTY_THREE = 23;
	public static final int TWENTY_FOUR = 24;
	public static final int TWENTY_FIVE = 25;
	public static final int THIRTY = 30;
	public static final int FORTY = 40;
	public static final int NINETY_NINE = 99;
	public static final int FIFTY = 50;
	public static final int ONE_HUNDRED_FIFTY = 150;
	public static final int THREE_DIGITS = 999;
	public static final int SIX_DIGITS = 999_999;
	public static final int SEVEN_DIGITS = 999_999_9;
	public static final long ELEVEN_DIGITS = 999_999_999_99L;
	/* Config: */
	public static final String RATE_MASTER = "rate-master";
	public static final String PRODUCT_MASTER = "product-master";
	/* Cache: */
	public static final String RATE_MASTER_EXCHANGE_RESPONSES = "rateMasterExchangeResponses";
	public static final String APPLICATION_DATE_TO_STRING = "#applicationDate.toString()";
	/* Query param: */
	public static final String EFFECTIVE_DATE_IN = "effectiveDateIn";
	/* Source: */
	public static final String BPM = "BPM";
	public static final String DIS = "DIS";
	public static final String ING = "ING";
	public static final String VAN = "VAN";
	/* Mapper: */
	public static final String ID = "id";
	public static final String UPDATE_DATE = "updateDate";
	public static final String EFFECTIVE_DATE = "effectiveDate";
	public static final String CANCELLATION_DATE = "cancellationDate";
	public static final String PENDING_POLICY_CLIENTS = "pendingPolicyClients";
	public static final String APPLICATION_DATE = "applicationDate";
	public static final String APPLICATION_NUMBER = "applicationNumber";
	public static final String DATA_SOURCE = "dataSource";
	public static final String TTM_RATE = "ttmRate";
	public static final String JAVA_TIME_LOCAL_DATE_NOW = "java(java.time.LocalDate.now())";
	public static final String JAVA_TIME_LOCAL_DATE_NOW_PLUS_DAYS_1 = "java(java.time.LocalDate.now().plusDays(1))";
	public static final String SET_PENDING_POLICY_CLIENT_INGENIUM_PENDING_POLICY = "java(setPendingPolicyClient(ingeniumPendingPolicy))";
	public static final String FORMAT_STRING_DATE = "formatStringDate";
	public static final String FORMAT_VANTAGE_TTM_RATE = "formatVantageTtmRate";
	public static final String SET_PENDING_POLICY_CLIENT_VANTAGE_PENDING_POLICY = "java(setPendingPolicyClient(vantagePendingPolicy))";
	public static final String SET_PENDING_POLICY_CLIENT_PENDING_POLICY = "java(setPendingPolicyClient(pendingPolicyDocumentSource))";
	public static final String WHITE_SPACE = " ";
	/* Date time format: */
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String YYYYMMDD = "yyyyMMdd";
	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	/* Currency: */
	public static final String JPY = "JPY";
	/* Pattern: */
	public static final String RATE_PATTERN = "^\\d+(\\.\\d+)?$";
	/* Message property: */
	public static final String CLASSPATH_MESSAGES_PROPERTIES = "classpath:messages.properties";
	public static final String COM_MANULIFE_AP_VALIDATION_DATE_DATE_VALIDATOR_MESSAGE = "{com.manulife.ap.validation.date.DateValidator.message}";

	public static final String COM_MANULIFE_AP_VALIDATION_PENDING_POLICY_VALIDATOR_MESSAGE =
			"{com.manulife.ap" +
			".validation.date.PendingPolicyDocumentValidator.message}";
	/* Log messages: */
	public static final String STARTED_WITH_ARGUMENT_S = "Started: {} with argument('s) {}";
	public static final String STARTED_PROCESS_BPM_AND_DIS = "Started: Flux com.manulife.ap.service.PendingPolicyIngestionServiceImpl.processBpmAndDis() with argument('s) []";
	public static final String STARTED_PROCESS_INGENIUM = "Started: Flux com.manulife.ap.service.PendingPolicyIngestionServiceImpl.processIngenium() with argument('s) []";
	public static final String STARTED_PROCESS_VANTAGE = "Started: Flux com.manulife.ap.service.PendingPolicyIngestionServiceImpl.processVantage() with argument('s) []";
	public static final String STARTED_PROCESS_CANCELLATION = "Started: Flux com.manulife.ap.service.PendingPolicyIngestionServiceImpl.processCancellation() with argument('s) []";
	public static final String ERROR_VALIDATE = "Error: Validate {} {}";
	public static final String STARTED_EXCHANGE_WITH_ARGUMENT = "Started: com.manulife.ap.service.client.ratemaster.RateMasterClientImpl.exchange(LocalDate) with argument('s) {}";
	public static final String COMMA = ",";
	public static final String COMPLETED_JOB = "Completed: Job";
	/* Aspect: */
	public static final String ANNOTATION_LOG_BEFORE = "@annotation(LogBefore)";
	/* Error messages: */
	public static final String APPLICATION_DATE_MUST_NOT_BE_NULL = "applicationDate must not be null";
	public static final String APPLICATION_DATE_IS_INVALID = "applicationDate is invalid";
	public static final String CURRENCY_IS_INVALID = "currency is invalid";
	public static final String PREMIUM_METHOD_MUST_NOT_BE_NULL = "premiumMethod must not be null";
	public static final String PREMIUM_METHOD_IS_INVALID = "premiumMethod is invalid";
	public static final String LEVEL_OR_LUMP_SUM_PREMIUM_MUST_NOT_BE_NULL = "levelOrLumpSumPremium must not be null";
	public static final String PREMIUM_MODE_MUST_NOT_BE_NULL = "premiumMode must not be null";
	public static final String PREMIUM_MODE_IS_INVALID = "premiumMode is invalid";
	public static final String PREMIUM_PAY_PERIOD_MUST_NOT_BE_NULL = "premiumPayPeriod must not be null";
	/* Others: */
	public static final String EMPTY_STRING = "";
	public static final String UNDERSCORE = "_";
	public static final String PERIOD = ".";
	public static final Void NULL_VOID = null;
	//public static final RateMasterExchangeResponse NULL_RATE_MASTER_EXCHANGE_RESPONSE = null;
	public static final String NO_PRODUCT_LIST_FOUND = "Empty product result list";
	public static final String NO_PREMIUM_PAY_PERIOD = "No premium pay period";
	public static final String NO_PAYMENT_CALC = "No paymentCalc";
	public static final String NO_PLAN_CODE = "No planCode";
	public static final String NO_INSURED_CLIENT = "No insured client";
	/* Document: */
	public static final String PEND_POLICY_CLIENTS = "pendPolicyClients";
	public static final String POLICYHOLDER_ANNUAL_INCOME = "policyholderAnnualIncome";
	public static final String POLICYHOLDER_FINANCIAL_ASSETS = "policyholderFinancialAssets";
	public static final String TOTAL_ANNUAL_PREMIUMS_TTMRATE = "totalAnnualPremiumsTTMRate";
	public static final String ESTIMATED_TOTAL_PREMIUMS_TTMRATE = "estimatedTotalPremiumsTTMRate";
	public static final String LUMP_SUM_PREMIUMS_TTMRATE = "lumpSumPremiumsTTMRate";
	public static final String TTMRATE = "TTMRate";
	public static final String LEVEL_ORLUMPSUM_PREMIUM = "levelOrlumpsumPremium";
	public static final String POLICY_ISSUED_FLG = "policyIssued_flag";
	/* Encoding: */
	public static final String SHIFT_JIS = "Shift_JIS";
	/* TimeZone: */
	public static final String JST = "JST";
	/* Name Kana Converter: */
	public static final char SMALL_A = 'a';
	public static final char BIG_A = 'A';
	public static final char SMALL_Z = 'z';
	public static final char BIG_Z = 'Z';
	public static final char CHAR_0 = '0';
	public static final char CHAR_9 = '9';
	public static final char JAPANESE_A = 'ｱ';
	public static final char JAPANESE_N = 'ﾝ';
	public static final String DATE_DELIMITER = "-";
	public static final String VALID_DATE_REGEX = "^\\d{4}-\\d{2}-\\d{2}$";
	public static final String DATE_REGEX = "^[0-9]{8}$";
	public static final String DATE_FORMATTER = "yyyy-MM-dd";
	public static final String INVALID_TOKEN = "Inavlid Token Provided";
	public static final String POLICY_ISSUED_VALUE = "Y";
	public static final String APPLICATIONDATE_DOB_NOTFOUND = "Application Date or Insured client Dob not found";
	public static final int MAX_AGE_CRITERIA = 99;
	/* Cache: */
	public static final String PRODUCT_MASTER_EXCHANGE_RESPONSES = "productMasterExchangeResponses";
	public static final String PLAN_CODE = "#planCode";
	public static final String BIRTH_DATE_IS_INVALID = "dateOfBirth is invalid";
}
