package com.spring.reactive.web.config;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentMethod {
	
	private List<Integer> levelList;
	
	private List<Integer> lumpSumList;

}
