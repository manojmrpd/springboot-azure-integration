package com.spring.reactive.web.service.impl;

import com.azure.storage.blob.models.BlobProperties;
import com.opencsv.bean.CsvToBeanBuilder;
import com.spring.reactive.web.client.BlobContainerClient;
import com.spring.reactive.web.config.property.ConfigurationPropertiesConfig;
import com.spring.reactive.web.document.PendingPolicyDocument;
import com.spring.reactive.web.opencsv.IngeniumPendingPolicy;
import com.spring.reactive.web.opencsv.PendingPolicy;
import com.spring.reactive.web.repository.PendingPolicyRepository;
import com.spring.reactive.web.validation.document.ConstraintViolationUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.spring.reactive.web.mapper.MapperUtil.MAPPER_UTIL_INSTANCE;

@Service
@Slf4j
@RequiredArgsConstructor
public class IngVanPendingPolicyIngestionService {

    private final PendingPolicyRepository pendingPolicyRepository;
    private final ConstraintViolationUtil constraintViolation;
    private final ConfigurationPropertiesConfig configurationPropertiesConfig;
    private final BlobContainerClient blobContainerClient;

    public Flux<PendingPolicyDocument> processIngenium() {
        log.info("STARTED INGENIUM INGESTION PROCESS");
        String file = configurationPropertiesConfig.getIngeniumFileName();
        return getValidatedPendingPoliciesFromFile(file, IngeniumPendingPolicy.class)
                .flatMapMany(ingeniumPendingPolicies -> getExistingPendingPolicies(ingeniumPendingPolicies)
                        .flatMapMany(pendingPolicyDocuments -> savePendingPolicies(ingeniumPendingPolicies,
                                pendingPolicyDocuments,
                                MAPPER_UTIL_INSTANCE::updatePendingPolicyDocumentFromIngeniumCsvBean,
                                MAPPER_UTIL_INSTANCE::ingeniumCsvBeanToPendingPolicyDocument)));
    }

    private <T extends PendingPolicy> Flux<PendingPolicyDocument> savePendingPolicies(Set<T> pendingPolicies,
                                                                                      Set<PendingPolicyDocument> pendingPolicyDocuments,
                                                                                      BiFunction<T, PendingPolicyDocument,
                                                                                              PendingPolicyDocument> biFunction,
                                                                                      Function<T, PendingPolicyDocument> function) {
        final Set<PendingPolicyDocument> pendingPolicyDocumentSet = new CopyOnWriteArraySet<>();
        pendingPolicies.forEach(pendingPolicy -> pendingPolicyDocuments.parallelStream().unordered()
                .filter(pendingPolicyDocument -> pendingPolicyDocument.getApplicationNumber().equals(pendingPolicy.getApplicationNumber()))
                .findAny().ifPresentOrElse(pendingPolicyDocument -> pendingPolicyDocumentSet.add(biFunction.apply(pendingPolicy, pendingPolicyDocument))
                        , () -> pendingPolicyDocumentSet.add(function.apply(pendingPolicy))));
        return pendingPolicyDocumentSet.isEmpty() ? Flux.empty() :
                pendingPolicyRepository.saveAll(pendingPolicyDocumentSet);

    }

    private <T extends PendingPolicy> Mono<CopyOnWriteArraySet<PendingPolicyDocument>> getExistingPendingPolicies(Set<T> pendingPolicies) {
        return pendingPolicyRepository.findByApplicationNumberIn(pendingPolicies.parallelStream().unordered()
                        .map(T::getApplicationNumber).collect(Collectors.toCollection(CopyOnWriteArraySet::new)))
                .collect(Collectors.toCollection(CopyOnWriteArraySet::new));
    }

    private <T extends PendingPolicy> Mono<Set<T>> getValidatedPendingPoliciesFromFile(String file,
                                                                                       Class<T> tClass) {
        String local = configurationPropertiesConfig.getLocal().concat(file);
        String directory = configurationPropertiesConfig.getIngeniumDirectory();
        return downloadToFile(directory.concat(file), local).flatMapIterable(blobProperties -> readBlob(local, tClass))
                .flatMap(constraintViolation::validate).collect(Collectors.toSet());
    }

    private Mono<BlobProperties> downloadToFile(String blob, String local) {
        return blobContainerClient.downloadToFile(blob, local);
    }

    @SneakyThrows
    private <T> List<T> readBlob(String file, Class<T> tClass) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(Path.of(file))) {
            return new CsvToBeanBuilder<T>(bufferedReader).withType(tClass).build().parse();

        }
    }
}
