package com.spring.reactive.web.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ElderlyCheckResponse {
	private String nameKanaConverted;
	private String applicationNumber;
	private BigDecimal annualIncome;
	private BigDecimal financialAssets;
	private BigDecimal totalAnnualPremium;
	private BigDecimal annualIncomeLimitForLevelPayment;
	private BigDecimal estimatedTotalPremium;
	private BigDecimal financialAssetsLimitForLevelPayment;
	private BigDecimal totalLumpSumPremium;
	private BigDecimal excessAnnualIncomeForLevelPayment;
	private BigDecimal excessFinancialAssetsForLevelPayment;
	private BigDecimal financialAssetsForLumpSumPayment;
	private BigDecimal excessFinancialAssetsForLumpSumPayment;
	private Long policyCountForOneYear;
	private Long policyCountForTwoYear;
	private Long policyCountForLevelPayment;
	private Long excessPolicyCountForOneYear;
	private Long excessPolicyCountForTwoYear;
	private Long excessPolicyCountForLevelPayment;
	private List<CheckResult> checkResult;
	private Integer systemErrorCode;
	private String systemErrorMessage;
	

}
