package com.spring.reactive.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveHistoryCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveHistoryCheckApplication.class, args);
	}

}
