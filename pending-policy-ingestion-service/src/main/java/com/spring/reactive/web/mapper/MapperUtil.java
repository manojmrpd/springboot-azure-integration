package com.spring.reactive.web.mapper;

import com.spring.reactive.web.document.PendingPolicyClientDocument;
import com.spring.reactive.web.document.PendingPolicyDocument;
import com.spring.reactive.web.opencsv.IngeniumPendingPolicy;
import com.spring.reactive.web.opencsv.VantagePendingPolicy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.util.Set;

import static com.spring.reactive.web.constant.PendingPolicyConstant.*;
import static java.util.Collections.emptySet;
import static java.util.Optional.ofNullable;
import static org.mapstruct.factory.Mappers.getMapper;

@Mapper
public interface MapperUtil {
    MapperUtil MAPPER_UTIL_INSTANCE = getMapper(MapperUtil.class);

    @Mapping(target = UPDATE_DATE, expression = JAVA_TIME_LOCAL_DATE_NOW)
    @Mapping(target = EFFECTIVE_DATE, expression = JAVA_TIME_LOCAL_DATE_NOW_PLUS_DAYS_1)
    @Mapping(target = PENDING_POLICY_CLIENTS, expression = SET_PENDING_POLICY_CLIENT_INGENIUM_PENDING_POLICY)
    @Mapping(target = APPLICATION_DATE, source = APPLICATION_DATE, qualifiedByName = FORMAT_STRING_DATE)
    @Mapping(target = DATA_SOURCE, constant = ING)
    @Mapping(target = ID, source = APPLICATION_NUMBER)
    @Mapping(target = APPLICATION_NUMBER, source = APPLICATION_NUMBER)
    PendingPolicyDocument ingeniumCsvBeanToPendingPolicyDocument(IngeniumPendingPolicy ingeniumPendingPolicy);

    @Mapping(target = UPDATE_DATE, expression = JAVA_TIME_LOCAL_DATE_NOW)
    @Mapping(target = PENDING_POLICY_CLIENTS, expression = SET_PENDING_POLICY_CLIENT_INGENIUM_PENDING_POLICY)
    @Mapping(target = APPLICATION_DATE, source = APPLICATION_DATE, qualifiedByName = FORMAT_STRING_DATE)
    @Mapping(target = DATA_SOURCE, constant = ING)
    PendingPolicyDocument updatePendingPolicyDocumentFromIngeniumCsvBean(
            IngeniumPendingPolicy ingeniumPendingPolicy,
            @MappingTarget PendingPolicyDocument pendingPolicyDocument);

    @Mapping(target = CANCELLATION_DATE, expression = JAVA_TIME_LOCAL_DATE_NOW_PLUS_DAYS_1)
    @Mapping(target = PENDING_POLICY_CLIENTS, expression = SET_PENDING_POLICY_CLIENT_PENDING_POLICY)
    PendingPolicyDocument updatePendingPolicyDocumentFromPendingPolicyDocument(
            PendingPolicyDocument pendingPolicyDocumentSource,
            @MappingTarget PendingPolicyDocument pendingPolicyDocumentTarget);

    default Set<PendingPolicyClientDocument> setPendingPolicyClient(final IngeniumPendingPolicy ingeniumPendingPolicy) {
        return Set.of(PendingPolicyClientDocument.builder().clientType(ONE).nameKanji(ingeniumPendingPolicy.getNameKanji())
                .nameKana(ingeniumPendingPolicy.getNameKana())
                .dateOfBirth(formatStringDate(ingeniumPendingPolicy.getDateOfBirth()))
                .gender(ingeniumPendingPolicy.getGender()).addressCode(ingeniumPendingPolicy.getAddressCode())
                .addressDetail(ingeniumPendingPolicy.getAddressDetail())
                .nameKanaConverted(ingeniumPendingPolicy.getNameKanaConverted()).build());
    }

    default Set<PendingPolicyClientDocument> setPendingPolicyClient(final VantagePendingPolicy vantagePendingPolicy) {
        return Set.of(PendingPolicyClientDocument.builder().clientType(ONE)
                .nameKanji(vantagePendingPolicy.getNameKanji().strip())
                .nameKana(vantagePendingPolicy.getNameKana().strip())
                .dateOfBirth(formatStringDate(vantagePendingPolicy.getDateOfBirth()))
                .gender(vantagePendingPolicy.getGender()).addressCode(vantagePendingPolicy.getAddressCode())
                .addressDetail(ofNullable(vantagePendingPolicy.getPrefecture()).orElse(EMPTY_STRING).concat(WHITE_SPACE)
                        .concat(ofNullable(vantagePendingPolicy.getCityName()).orElse(EMPTY_STRING)).concat(WHITE_SPACE)
                        .concat(ofNullable(vantagePendingPolicy.getAddr1()).orElse(EMPTY_STRING)).concat(WHITE_SPACE)
                        .concat(ofNullable(vantagePendingPolicy.getAddressDetail()).orElse(EMPTY_STRING)).strip())
                .nameKanaConverted(vantagePendingPolicy.getNameKanaConverted())
                .build());
    }

    default Set<PendingPolicyClientDocument> setPendingPolicyClient(final PendingPolicyDocument pendingPolicyDocument) {
        return ofNullable(pendingPolicyDocument.getPendingPolicyClients()).orElse(emptySet());
    }

    @Named(FORMAT_STRING_DATE)
    default String formatStringDate(final String date) {
        return date.substring(0, 4) + DATE_DELIMITER + date.substring(4, 6) + DATE_DELIMITER + date.substring(6, 8);
    }

    @Named(FORMAT_VANTAGE_TTM_RATE)
    default String formatVantageTtmRate(final String ttmRate) {
        return new BigDecimal(ttmRate).movePointLeft(TWO).toString();
    }
}