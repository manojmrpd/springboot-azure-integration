package com.spring.reactive.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class PolicyClientDto {

    private String policyNumber;
    private String nameKana;
    private String birthDate;
    private Integer gender;


}
