package com.spring.reactive.web.client.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ElderlyCheckRequest {
	
	private String nameKana;
	private Integer gender;
	private LocalDate birthDate;
	private BigDecimal annualIncome;
	private BigDecimal financialAssets;
	private Set<ApplicationInfo> applicationInfo;

}
