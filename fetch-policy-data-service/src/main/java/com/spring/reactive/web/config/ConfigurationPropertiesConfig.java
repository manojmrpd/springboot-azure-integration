package com.spring.reactive.web.config;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.spring.reactive.web.constant.HistoryCheckConstant;
import com.spring.reactive.web.dto.PremiumMode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.util.Set;

@Component
@Builder
@Data
@ConfigurationProperties(HistoryCheckConstant.CUSTOM)
@Validated
@NoArgsConstructor
@AllArgsConstructor
public class ConfigurationPropertiesConfig {
	
	@Valid
	@NotNull
	private PaymentMethod paymentMethods;

	private Set<PremiumMode> premiumModes;

	private Set<String> policyStatus;
	
	

}
