package com.spring.reactive.web.constant;

public class HistoryCheckConstant {

	public static final String CUSTOM = "custom";
	public static final String HISTORY_CHECK = "/jcus/api/v1/history-check";
	public static final String HISTORY_CHECK_SOAP = "/jcus/api/v1/history-check-soap";
	public static final String JCUS = "/jcus/**";
	public static final String ACTUATOR = "/actuator/**";
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SIX = 6;
	public static final int SEVEN = 7;
	public static final int NINE = 9;
	public static final int ELEVEN = 11;
	public static final int TWELVE = 12;
	public static final int DIVISOR_TWELVE = 12;
	public static final int THIRTEEN = 13;
	public static final int FIFTEEN = 15;
	public static final int SIXTY = 60;
	public static final int NINETY_NINE = 99;
	public static final Integer ING_LARGE_TYPE_CODE = 500;
	public static final String PENDING_POLICIES = "pending-policies";
	public static final String STEREOTYPE_POINTCUT_EXPRESSION = "@within(org.springframework.stereotype.Repository)"
			+ " || @within(org.springframework.stereotype.Service)"
			+ " || @within(org.springframework.web.bind.annotation.RestController)";
	public static final String CONTROLLER_ADVICE_POINTCUT_EXPRESSION = "@within(org.springframework.web.bind.annotation.RestControllerAdvice)";
	public static final String STARTED_WITH_ARGUMENTS = "Started: {} with arguments = {}";
	public static final String COMPLETED_WITH_RETURN = "Completed: {} with return = {}";
	public static final String EXCEPTION = "Exception: {} with message = {}";
	public static final String CLASSPATH_MESSAGES_PROPERTIES = "classpath:messages.properties";
	public static final String ANNUAL_OR_LUMP_SUM_PREMIUM_MESSAGE = "{com.manulife.ap.validation.premium.AnnualOrLumpSumPremium.message}";
	public static final String PAYMENT_MODE_MESSAGE = "{com.manulife.ap.validation.paymentmode.PaymentMode.message}";
	public static final String GENDER_MESSAGE = "{com.manulife.ap.validation.gender.Gender.message}";
	public static final String MANDATORY_ITEM_CHECK_ERROR_MESSAGE = "リクエスト必須項目チェックエラー: 項目名 %s = %s";
	public static final String ITEM_ATTRIBUTE_CHECK_ERROR_MESSAGE = "リクエスト項目属性チェックエラーの時: エラー項目名 %s = %s";
	public static final String DB_CONNECTION_ERROR_MESSAGE = "DB接続エラーが発生しました。システム管理者へご連絡下さい。";
	public static final String SYSTEM_ERROR_MESSAGE = "システムエラーが発生しました。システム管理者へご連絡下さい。";
	public static final String HISTORY_CHECK_PARAMETERS = "history-check-parameters";
	public static final String SCV_STATUS = "scvStatus";
	public static final String CONDITION_VALUES = "condition-values";
	public static final String LOGGING_POINTCUT_EXPRESSION = "@within(org.springframework.stereotype.Repository)"
			+ " || @within(org.springframework.stereotype.Service)"
			+ " || @within(org.springframework.web.bind.annotation.RestController)"
			+ " || @within(org.springframework.web.bind.annotation.RestControllerAdvice)";
	public static final String HEADER_XCLIENTAPP = "X-CLIENT-APP";
	public static final String HEADER_XCLIENTAPP_VAL = "APOLLO";
	public static final String JCMS_WEBCLIENT = "jcms-webclient";
	public static final String POLICY_MASTER_DATA = "policy-master-data";
	public static final String LEVEL_INFORCED = "level_inforced";
	public static final String LUMPSUM_INFORCED = "lumpSum_inforced";
	public static final String LEVEL_ENFORCED_TOTAL_ESTIMATED = "level_enforced_total_estimated";
	public static final String LEVEL_PENDING_TOTAL_ESTIMATED = "level_pending_total_estimated";
	public static final String LEVEL_PENDING = "level_pending";
	public static final String LUMPSUM_PENDING = "lumpSum_pending";
	public static final String LEVEL_CURRENT = "level_current";
	public static final String LUMPSUM_CURRENT = "lumpSum_current";
	public static final String LEVEL_CURRENT_TOTAL_ESTIMATED = "level_current_total_estimated";
	public static final String LUMPSUM_CURRENT_TOTAL_ESTIMATED = "lumpSum_current_total_estimated";
	public static final String ENFORCED_COMMON_SET = "enforced_common_set";
	public static final String PENDING_COMMON_SET = "pending_common_set";
	public static final String ENFORCED_LUMPSUM_SET = "enforced_lumpSum_set";
	public static final String ENFORCED_LEVEL_SET = "enforced_level_set";
	public static final String PENDING_LUMPSUM_SET = "pending_lumpSum_set";
	public static final String PENDING_LEVEL_SET = "pending_level_set";
	public static final String APPLIED_LEVEL_SET = "pending_level_set";
	public static final String APPLIED_LUMPSUM_SET = "pending_level_set";
	public static final String THROWABLE = "throwable";
	public static final String INVALID_TOKEN = "Inavlid Token Provided to policy master";
	public static final String EXCEPTION_WITH_CAUSE = "Exception: {} with cause = {}";
	public static final String PAYMENT_METHOD_MESSAGE = "{com.manulife.ap.validation.paymentmode.PolicyMasterPayment.message}";
	public static final String INVALID_REQUEST = "Invalid request provided to policy master";
	public static final String DATA_NOT_FOUND = "Data not found";
	public static final String SOMETHING_WENT_WRONG = "Something went wrong while calling policy master";
	public static final String PENDING_POLICYCLIENTS = "pendPolicyClients";
	public static final String SIMPLE_NAME_KANA_CONVERTED = "nameKanaConverted";
	public static final String SIMPLE_BIRTH_DATE = "dateOfBirth";
	public static final String SIMPLE_GENDER = "gender";
	/* Rate Master: */
	public static final String RATE_MASTER = "rate-master";
	public static final String RATE_MASTER_DATA = "rate-master-data";
	public static final String STARTED_EXCHANGE_WITH_ARGUMENT = "Started: com.manulife.ap.service.client.ratemaster.RateMasterClientImpl.exchange(LocalDate) with argument('s) {}";
	public static final String RATE_MASTER_EXCHANGE_RESPONSES = "rateMasterExchangeResponses";
	public static final String APPLICATION_DATE_TO_STRING = "#applicationDate.toString()";
	/* Query param: */
	public static final String EFFECTIVE_DATE_IN = "effectiveDateIn";
	/* Currency: */
	public static final String JPY = "JPY";
	/* Pattern: */
	public static final String RATE_PATTERN = "^\\d+(\\.\\d+)?$";
	/* Source */
	public static final String ING = "ING";
	public static final String VAN = "VAN";
	/* Document: */
	public static final String PEND_POLICY_CLIENTS = "pendPolicyClients";
	public static final String POLICYHOLDER_ANNUAL_INCOME = "policyholderAnnualIncome";
	public static final String POLICYHOLDER_FINANCIAL_ASSETS = "policyholderFinancialAssets";
	public static final String TOTAL_ANNUAL_PREMIUMS_TTMRATE = "totalAnnualPremiumsTTMRate";
	public static final String ESTIMATED_TOTAL_PREMIUMS_TTMRATE = "estimatedTotalPremiumsTTMRate";
	public static final String LUMP_SUM_PREMIUMS_TTMRATE = "lumpSumPremiumsTTMRate";
	public static final String TTMRATE = "TTMRate";
	public static final String LEVEL_ORLUMPSUM_PREMIUM = "levelOrlumpsumPremium";
	public static final String LUMPSUM_MODE = "99";

	public static final String LUMPSUM_MODE_UNISYS = "1";
	public static final String DEFAULT_VALUE = "0";
	public static final String POLICY_STATUS = "1";
	public static final String DIS = "DIS";
	public static final String EIGHT = "8";
	public static final String TRAD_PAID_DATE = "0001-01-01";
	public static final String SUL_COLLECTION_CATEGORY_CODE = "DEP";
	public static final String SUL_COLLECTION_METHOD_CODE = "MAT";
	public static final String LEVEL_PRODUCT_TYPE_CODE = "P1";
	public static final String CANCELLATION_DATE = "cancellationDate";
	public static final Integer RIDER_CODE = 1;
	public static final String POLICY_ISSUED_FLG = "policyIssued_flag";
	public static final String BPM = "BPM";
	/* OCDM Config */
	public static final String OCDM_DATA = "ocdm-data";
	public static final String OCDM = "ocdm";
}
