package com.spring.reactive.web.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.spring.reactive.web.constant.EnforcedPolicyConstant;
import com.spring.reactive.web.controller.dto.*;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

@Data
@Builder
@Document(collection = EnforcedPolicyConstant.POLICIES)
public class Policies {
    @Id
    private String id;
    private PolicyInfo policy;
}
