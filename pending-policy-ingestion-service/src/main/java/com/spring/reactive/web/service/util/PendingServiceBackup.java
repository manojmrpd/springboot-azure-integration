/*
package com.spring.reactive.web.service.util;

import com.spring.reactive.web.document.PendingPolicyDocument;
import com.spring.reactive.web.repository.PendingPolicyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.lang.Integer.parseInt;
import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;
import static java.time.Duration.of;
import static java.time.LocalDate.now;
import static java.time.LocalDate.parse;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toCollection;
import static lombok.AccessLevel.PACKAGE;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static reactor.core.publisher.Flux.fromIterable;
import static reactor.core.publisher.Mono.*;

@RequiredArgsConstructor(access = PACKAGE)
@Service
@Log4j2
public class PendingServiceBackup {
   // private final RateMasterClient rateMasterClient;
    //private final BatchHistoryCheckClient batchHistoryCheckClient;
    private final ProductMasterClientImpl productMasterClient;
    private final PendingPolicyRepository pendingPolicyRepository;
    private final ConfigurationPropertiesConfig configurationPropertiesConfig;
    private final BlobContainerClient blobContainerClient;
    private final Validator validator;
    private final NameKanaConverterService nameKanaConverterService;

    @LogBefore
    @Override
    public Mono<Void> ingest() {
        return fromCallable(this::process);
    }

    public Void process() {
        final var file = configurationPropertiesConfig.getFile();
        final String localDirectory = file.getLocalDirectory();
        final String backupDirectory = file.getBackupDirectory();
        final var ingeniumFileProperty = file.getIngenium();
        final String ingeniumFilename = ingeniumFileProperty.getFilename();
        final var vantageFileProperty = file.getVantage();
        final String vantageFilename = vantageFileProperty.getFilename();
        final String dateTime = UNDERSCORE.concat(ofPattern(YYYYMMDDHHMMSS).format(LocalDateTime.now()));
        processBpmAndDis().doOnComplete(() -> processVantage().doOnComplete(() -> processIngenium()
                .doOnComplete(() -> uploadFromFile(getBackupFilename(backupDirectory, vantageFilename, dateTime),
                        localDirectory.concat(vantageFilename))
                        .doOnSuccess((final Void unused0) -> uploadFromFile(
                                getBackupFilename(backupDirectory, ingeniumFilename, dateTime),
                                localDirectory.concat(ingeniumFilename))
                                .doOnSuccess((final Void unused1) -> delete(vantageFileProperty
                                        .getDirectory().concat(vantageFilename)).doOnSuccess(
                                                (final Void unused2) -> delete(ingeniumFileProperty
                                                        .getDirectory().concat(ingeniumFilename))
                                                        .doOnSuccess((
                                                                final Void unused3) -> processCancellation()
                                                                .doOnComplete(
                                                                        () -> batchHistoryCheckClient
                                                                                .process()
                                                                                .doOnSuccess(
                                                                                        (final Void unused4) -> log
                                                                                                .info(COMPLETED_JOB))
                                                                                .subscribe())
                                                                .subscribe())
                                                        .subscribe())
                                        .subscribe())
                                .subscribe())
                        .subscribe())
                .subscribe()).subscribe()).subscribe();
        return NULL_VOID;
    }

    private Flux<PendingPolicyDocument> processBpmAndDis() {
        log.info(STARTED_PROCESS_BPM_AND_DIS);
        return pendingPolicyRepository
                .findByDataSourceInAndUpdateDateAndCurrencyCodeIn(configurationPropertiesConfig.getSources()
                        .parallelStream().unordered().filter((final Source source) -> !source.getCurrencies().isEmpty())
                        .map(Source::getName).collect(toCollection(CopyOnWriteArraySet::new)), now(), getCurrencies())
                .retryWhen(getExceptionRetrySpec(getMongoServerExceptionPredicate())).flatMap(this::validate)
                .flatMap(this::calculateDataFromBpmAndDis).collect(toCollection(CopyOnWriteArraySet::new))
                .filter(pendingPolicyDocuments -> !pendingPolicyDocuments.isEmpty()).flatMapMany(this::saveAll);
    }

    private Flux<PendingPolicyDocument> processIngenium() {
        log.info(STARTED_PROCESS_INGENIUM);
        return getValidatedPendingPoliciesFromFile(configurationPropertiesConfig.getFile().getIngenium(),
                IngeniumPendingPolicy.class).flatMapMany(
                (final Set<IngeniumPendingPolicy> ingeniumPendingPolicies) -> getExistingPendingPolicies(
                        ingeniumPendingPolicies).flatMapMany(
                        (final Set<PendingPolicyDocument> pendingPolicyDocuments) -> savePendingPolicies(
                                ingeniumPendingPolicies, pendingPolicyDocuments,
                                MAPPER_UTIL_INSTANCE::updatePendingPolicyDocumentFromIngeniumPendingPolicy,
                                MAPPER_UTIL_INSTANCE::ingeniumPendingPolicyToPendingPolicyDocument)));
    }

    private Flux<PendingPolicyDocument> processVantage() {
        log.info(STARTED_PROCESS_VANTAGE);
        return getValidatedPendingPoliciesFromFile(configurationPropertiesConfig.getFile().getVantage(),
                VantagePendingPolicy.class).flatMapMany(
                (final Set<VantagePendingPolicy> vantagePendingPolicies) -> getExistingPendingPolicies(
                        vantagePendingPolicies).flatMapMany(
                        (final Set<PendingPolicyDocument> pendingPolicyDocuments) -> savePendingPolicies(
                                vantagePendingPolicies, pendingPolicyDocuments,
                                MAPPER_UTIL_INSTANCE::updatePendingPolicyDocumentFromVantagePendingPolicy,
                                MAPPER_UTIL_INSTANCE::vantagePendingPolicyToPendingPolicyDocument)));
    }

    private <T extends PendingPolicy> Mono<CopyOnWriteArraySet<T>> getValidatedPendingPoliciesFromFile(
            final FileProperty fileProperty, final Class<T> tClass) {
        final String filename = fileProperty.getFilename();
        final String localFilePath = configurationPropertiesConfig.getFile().getLocalDirectory().concat(filename);
        return downloadToFile(fileProperty.getDirectory().concat(filename), localFilePath)
                .flatMapIterable((final BlobProperties blobProperties) -> readBlob(localFilePath, tClass))
                .flatMap(this::validate).flatMap(this::setNameKanaConverted)
                .collect(toCollection(CopyOnWriteArraySet::new));
    }

    private <T extends PendingPolicy> Mono<T> setNameKanaConverted(T t) {
        t.setNameKanaConverted(nameKanaConverterService.convertNameKana(t.getNameKana()));
        return just(t);
    }

    private <T extends PendingPolicy> Flux<PendingPolicyDocument> savePendingPolicies(final Set<T> pendingPolicies,
                                                                                      final Set<PendingPolicyDocument> pendingPolicyDocuments,
                                                                                      final BiFunction<T, PendingPolicyDocument, PendingPolicyDocument> biFunction,
                                                                                      final Function<T, PendingPolicyDocument> function) {
        final Set<PendingPolicyDocument> pendingPolicyDocumentSet = new CopyOnWriteArraySet<>();
        pendingPolicies.forEach((final T pendingPolicy) -> pendingPolicyDocuments.parallelStream().unordered()
                .filter((final PendingPolicyDocument pendingPolicyDocument) -> pendingPolicyDocument
                        .getApplicationNumber().equals(pendingPolicy.getApplicationNumber()))
                .findAny().ifPresentOrElse(
                        (final PendingPolicyDocument pendingPolicyDocument) -> pendingPolicyDocumentSet
                                .add(biFunction.apply(pendingPolicy, pendingPolicyDocument)),
                        () -> pendingPolicyDocumentSet.add(function.apply(pendingPolicy))));
        return pendingPolicyDocumentSet.isEmpty() ? Flux.empty() : saveAll(pendingPolicyDocumentSet);
    }

    private <T extends PendingPolicy> Mono<CopyOnWriteArraySet<PendingPolicyDocument>> getExistingPendingPolicies(
            final Set<T> pendingPolicies) {
        return pendingPolicyRepository
                .findByIdIn(pendingPolicies.parallelStream().unordered().map(T::getApplicationNumber)
                        .collect(toCollection(CopyOnWriteArraySet::new)))
                .retryWhen(getExceptionRetrySpec(getMongoServerExceptionPredicate()))
                .collect(toCollection(CopyOnWriteArraySet::new));
    }

    private Flux<PendingPolicyDocument> processCancellation() {
        log.info(STARTED_PROCESS_CANCELLATION);
        final var localDateNow = now();
        return pendingPolicyRepository
                .findByCancellationDateIsNullAndEffectiveDateLessThanEqualAndUpdateDateLessThan(localDateNow,
                        localDateNow)
                .retryWhen(getExceptionRetrySpec(getMongoServerExceptionPredicate()))
                .flatMap((final PendingPolicyDocument pendingPolicyDocument) -> just(
                        MAPPER_UTIL_INSTANCE.updatePendingPolicyDocumentFromPendingPolicyDocument(pendingPolicyDocument,
                                pendingPolicyDocument)))
                .collect(toCollection(CopyOnWriteArraySet::new))
                .filter((final Set<PendingPolicyDocument> pendingPolicyDocuments) -> !pendingPolicyDocuments.isEmpty())
                .flatMapMany(this::saveAll);
    }

    private Set<String> getCurrencies() {
        return configurationPropertiesConfig.getSources().parallelStream().unordered()
                .flatMap((final Source source) -> source.getCurrencies().parallelStream().unordered())
                .collect(toCollection(CopyOnWriteArraySet::new));
    }

    private <T> Publisher<T> validate(final T object) {
        final Set<ConstraintViolation<T>> violations = new HashSet<>(validator.validate(object));
        if (violations.isEmpty()) {
            return just(object);
        } else {
            final String message = violations.parallelStream().unordered().map(Object::toString)
                    .collect(joining(COMMA));
            log.error(ERROR_VALIDATE, object, message);
            return empty();
        }
    }

    private Publisher<PendingPolicyDocument> calculateDataFromBpmAndDis(
            final PendingPolicyDocument pendingPolicyDocument) {
        final String currencyPay = pendingPolicyDocument.getCurrencyCode();
        if (getCurrencies().parallelStream().unordered().filter((final String currency) -> !currency.equals(JPY))
                .anyMatch((final String currency) -> currency.equals(currencyPay))) {
            final var localDateNow = now();
            final LocalDate convertedApplicationDate = parse(pendingPolicyDocument.getApplicationDate());
            return rateMasterClient
                    .exchange(convertedApplicationDate.isAfter(localDateNow) ? localDateNow : convertedApplicationDate)
                    .flatMap(this::validate)
                    .flatMap((final RateMasterExchangeResponse rateMasterExchangeResponse) -> updatePendingPolicy(
                            pendingPolicyDocument, rateMasterExchangeResponse));
        } else {
            return updatePendingPolicy(pendingPolicyDocument, NULL_RATE_MASTER_EXCHANGE_RESPONSE);
        }
    }

    private Mono<PendingPolicyDocument> updatePendingPolicy(final PendingPolicyDocument pendingPolicyDocument,
                                                            final RateMasterExchangeResponse rateMasterExchangeResponse) {
        final Long levelOrLumpSumPremium = pendingPolicyDocument.getLevelOrLumpSumPremium();
        final String currencyPay = pendingPolicyDocument.getCurrencyCode();
        if (pendingPolicyDocument.getDataSource().equals(BPM)) {
            if (configurationPropertiesConfig.getPremium().getLevel().getPremiumMethods()
                    .contains(pendingPolicyDocument.getPremiumMethod())) {
                return getPremiumPayPeriod(pendingPolicyDocument).map(payPeriod -> {
                    if (!isNotACompleteRecord(payPeriod)) {
                        return getUpdatedBPMLevelPolicyRecord(pendingPolicyDocument, rateMasterExchangeResponse,
                                levelOrLumpSumPremium, currencyPay, payPeriod);
                    } else {
                        log.info("NOT UPDATED APPLICATION NUMBER :: {}", pendingPolicyDocument.getApplicationNumber());
                        return pendingPolicyDocument;
                    }
                });
            } else if (!currencyPay.equals(JPY)) {
                return updateLumpSumPremiumForBPMnonJPY(pendingPolicyDocument, rateMasterExchangeResponse,
                        levelOrLumpSumPremium);
            }
        } else if (!currencyPay.equals(JPY)) {
            return updateLumpSumPremiumForDISnonJPY(pendingPolicyDocument, rateMasterExchangeResponse,
                    levelOrLumpSumPremium);
        }
        return just(pendingPolicyDocument);
    }

    private PendingPolicyDocument getUpdatedBPMLevelPolicyRecord(final PendingPolicyDocument pendingPolicyDocument,
                                                                 final RateMasterExchangeResponse rateMasterExchangeResponse, final Long levelOrLumpSumPremium,
                                                                 final String currencyPay, String payPeriod) {
        String annualPremium = getTotalAnnualPremiumForBPM(pendingPolicyDocument, rateMasterExchangeResponse,
                levelOrLumpSumPremium, currencyPay);
        log.info("UPDATED APPLICATION NUMBER :: {}", pendingPolicyDocument.getApplicationNumber());
        pendingPolicyDocument.setTotalAnnualPremiumsTtmRate(annualPremium);
        return pendingPolicyDocument.toBuilder()
                .estimatedTotalPremiumsTtmRate(new BigDecimal(pendingPolicyDocument.getTotalAnnualPremiumsTtmRate())
                        .multiply(new BigDecimal(payPeriod)).toString())
                .build();
    }

    private String getTotalAnnualPremiumForBPM(final PendingPolicyDocument pendingPolicyDocument,
                                               final RateMasterExchangeResponse rateMasterExchangeResponse, final Long levelOrLumpSumPremium,
                                               final String currencyPay) {
        return currencyPay.equals(JPY)
                ? calculateTotalAnnualPremiums(valueOf(levelOrLumpSumPremium), pendingPolicyDocument)
                : calculateTotalAnnualPremiums(
                convertToJpy(pendingPolicyDocument, rateMasterExchangeResponse, levelOrLumpSumPremium)
                        .setScale(ZERO, HALF_UP),
                pendingPolicyDocument);
    }

    private Mono<PendingPolicyDocument> updateLumpSumPremiumForDISnonJPY(
            final PendingPolicyDocument pendingPolicyDocument,
            final RateMasterExchangeResponse rateMasterExchangeResponse, final Long levelOrLumpSumPremium) {
        pendingPolicyDocument.setLumpSumPremiumsTtmRate(
                convertToJpyWithoutMovingPoint(pendingPolicyDocument, rateMasterExchangeResponse, levelOrLumpSumPremium)
                        .setScale(ZERO, HALF_UP).toString());
        return just(pendingPolicyDocument);
    }

    private Mono<PendingPolicyDocument> updateLumpSumPremiumForBPMnonJPY(
            final PendingPolicyDocument pendingPolicyDocument,
            final RateMasterExchangeResponse rateMasterExchangeResponse, final Long levelOrLumpSumPremium) {
        pendingPolicyDocument.setLumpSumPremiumsTtmRate(
                convertToJpyWithoutMovingPoint(pendingPolicyDocument, rateMasterExchangeResponse, levelOrLumpSumPremium)
                        .setScale(ZERO, HALF_UP).toString());
        return just(pendingPolicyDocument);
    }

    private static boolean isNotACompleteRecord(String payPeriod) {
        return payPeriod.equals(APPLICATIONDATE_DOB_NOTFOUND) || payPeriod.equals(NO_PRODUCT_LIST_FOUND)
                || payPeriod.equals(NO_PREMIUM_PAY_PERIOD) || payPeriod.equals(NO_PAYMENT_CALC)
                || payPeriod.equals(NO_INSURED_CLIENT) || payPeriod.equals(NO_PLAN_CODE);
    }

    private Mono<String> getPremiumPayPeriod(final PendingPolicyDocument pendingPolicyDocument) {
        log.info("Calling product master service for application number {} and plan code {} ",
                pendingPolicyDocument.getApplicationNumber(), pendingPolicyDocument.getPlanCode());
        return null != pendingPolicyDocument.getPlanCode()
                ? productMasterClient.callProductMaster(pendingPolicyDocument.getPlanCode()).map(
                productListResult -> getPremiumPayPeriodWithPlanCode(productListResult, pendingPolicyDocument))
                : Mono.just(NO_PLAN_CODE);
    }

    public static String getPremiumPayPeriodWithPlanCode(ProductListResult productListResult,
                                                         PendingPolicyDocument pendingPolicyDocument) {
        if (null != productListResult && null != productListResult.getResults()) {
            Optional<ProductDetailModel> productList = productListResult.getResults().stream()
                    .filter(getProductDetailPredicate(pendingPolicyDocument.getPlanCode())).findFirst();
            if (!productList.isPresent()) {
                return NO_PRODUCT_LIST_FOUND;
            }
            return productList.get().getPlanCodeDetails().stream()
                    .filter(getPlanCodeDetailPredicate(pendingPolicyDocument.getPlanCode())).findFirst()
                    .map(details -> derivePremiumPayPeriod(details, pendingPolicyDocument)).get();
        }
        return NO_PRODUCT_LIST_FOUND;
    }

    private static Predicate<ProductDetailModel> getProductDetailPredicate(String planCode) {
        return result -> isNotEmpty(result.getPlanCodeDetails())
                && result.getPlanCodeDetails().stream().anyMatch(getPlanCodeDetailPredicate(planCode));
    }

    private static String derivePremiumPayPeriod(PlanCodeDetailsModel planCodeModel,
                                                 PendingPolicyDocument pendingPolicyDocument) {
        if (isBlank(planCodeModel.getPaymentCalc())) {
            log.info(NO_PAYMENT_CALC);
            return NO_PAYMENT_CALC;
        }
        if (isBlank(pendingPolicyDocument.getPremiumPayPeriod())) {
            log.info(NO_PREMIUM_PAY_PERIOD);
            return NO_PREMIUM_PAY_PERIOD;
        }
        int premiumPayPeriodInt = parseInt(pendingPolicyDocument.getPremiumPayPeriod());
        if (parseInt(planCodeModel.getPaymentCalc()) == ONE
                || (parseInt(planCodeModel.getPaymentCalc()) == NINE && premiumPayPeriodInt <= FORTY))
            return pendingPolicyDocument.getPremiumPayPeriod();
        if (parseInt(planCodeModel.getPaymentCalc()) == TWO || parseInt(planCodeModel.getPaymentCalc()) == THREE
                || (parseInt(planCodeModel.getPaymentCalc()) == NINE && premiumPayPeriodInt > FORTY)) {
            Optional<PendingPolicyClientDocument> insuredClientRecord = pendingPolicyDocument.getPendingPolicyClients()
                    .stream().filter(client -> client.getClientType() == TWO).findAny();
            if (!insuredClientRecord.isPresent()) {
                log.info(NO_INSURED_CLIENT);
                return NO_INSURED_CLIENT;
            }
            int toBeSubtractedFrom = ZERO;
            if (parseInt(planCodeModel.getPaymentCalc()) == TWO || (parseInt(planCodeModel.getPaymentCalc()) == NINE
                    && premiumPayPeriodInt > FORTY && premiumPayPeriodInt < NINETY_NINE)) {
                toBeSubtractedFrom = premiumPayPeriodInt;
            }
            if (parseInt(planCodeModel.getPaymentCalc()) == THREE
                    || (parseInt(planCodeModel.getPaymentCalc()) == NINE && premiumPayPeriodInt >= NINETY_NINE)) {
                toBeSubtractedFrom = MAX_AGE_CRITERIA;
            }
            return calculatePremiumPayPeriod(pendingPolicyDocument, insuredClientRecord, toBeSubtractedFrom);
        }
        return pendingPolicyDocument.getPremiumPayPeriod();
    }

    private static String calculatePremiumPayPeriod(PendingPolicyDocument pendingPolicyDocument,
                                                    Optional<PendingPolicyClientDocument> insuredClientRecord, Integer toBeSubtractedFrom) {
        log.info("STARTED:: calculatePremiumPayPeriod method");
        if (null == pendingPolicyDocument.getApplicationDate() || null == insuredClientRecord.get().getDateOfBirth()) {
            log.info(APPLICATIONDATE_DOB_NOTFOUND);
            return APPLICATIONDATE_DOB_NOTFOUND;
        }
        return Integer.valueOf(toBeSubtractedFrom - Period
                .between(LocalDate.parse(insuredClientRecord.get().getDateOfBirth(), ofPattern(DATE_FORMATTER)),
                        LocalDate.parse(pendingPolicyDocument.getApplicationDate(), ofPattern(DATE_FORMATTER)))
                .getYears()).toString();
    }

    private static Predicate<PlanCodeDetailsModel> getPlanCodeDetailPredicate(String planCode) {
        return planCodeDetail -> null != planCodeDetail && (null != planCode && null != planCodeDetail.getPlanCode()
                && planCodeDetail.getPlanCode().equals(planCode));
    }

    private String calculateTotalAnnualPremiums(final BigDecimal levelOrLumpSumPremium,
                                                final PendingPolicyDocument pendingPolicyDocument) {
        return levelOrLumpSumPremium.multiply(valueOf(configurationPropertiesConfig.getPremium().getLevel()
                .getPremiumModes().parallelStream().unordered()
                .filter((final PremiumMode premiumMode) -> premiumMode.getMode()
                        .equals(pendingPolicyDocument.getPremiumMode()))
                .findAny().orElseThrow().getPaymentPerYear())).setScale(ZERO, HALF_UP).toString();
    }

    private Mono<BlobProperties> downloadToFile(final String blobName, final String filePath) {
        return blobContainerClient.downloadToFile(blobName, filePath);
    }

    private Mono<Void> uploadFromFile(final String blobName, final String filePath) {
        return blobContainerClient.uploadFromFile(blobName, filePath);
    }

    private Mono<Void> delete(final String blobName) {
        return blobContainerClient.delete(blobName);
    }

    private Flux<PendingPolicyDocument> saveAll(final Iterable<PendingPolicyDocument> entities) {
        final var batch = configurationPropertiesConfig.getBatch();
        return fromIterable(entities).buffer(batch.getBuffer())
                .concatMap(pendingPolicyDocuments -> delay(of(batch.getDelay(), SECONDS))
                        .thenMany(pendingPolicyRepository.saveAll(pendingPolicyDocuments)
                                .retryWhen(getExceptionRetrySpec(getMongoServerExceptionPredicate()))));
    }

    @Override
    public Mono<Void> delete() {
        return fromCallable(this::deleteIssuedPolicy);
    }

    public Void deleteIssuedPolicy() {
        LocalDate actualDate = LocalDate.now().minusDays(90);
        Flux<PendingPolicyDocument> pendingPolicyDocument = pendingPolicyRepository
                .findByCancellationDateLessThanEqual(actualDate);
        pendingPolicyRepository.deleteAll(pendingPolicyDocument).subscribe();
        return NULL_VOID;
    }
}*/
