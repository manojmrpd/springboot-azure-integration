package com.spring.reactive.web.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@Builder
@Getter
@Setter
@NoArgsConstructor
public final class Coverage {
    @JsonProperty("maturityExpiryDate")
    private String maturityExpiryDate;
    @JsonProperty("ulPaidUpDate")
    private String ulPaidUpDate;
    @JsonProperty("riderCode")
    private Integer riderCode;
}