package com.spring.reactive.web.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.spring.reactive.web.service.PendingPolicyService;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class PendingPolicyIngestionServiceImpl implements PendingPolicyService {

    private final BpmDisPendingPolicyIngestionService bpmDisPendingPolicyIngestionService;
    private final IngVanPendingPolicyIngestionService ingVanPendingPolicyIngestionService;

    @Override
    public Mono<Void> getPendingPolicies() {
        return Mono.fromCallable(this::process);
    }

    public Void process() {
        log.info("started pending policies ingestion service");
        bpmDisPendingPolicyIngestionService.processBpmAndDis()
                .doOnComplete(() -> ingVanPendingPolicyIngestionService.processIngenium()
                        .doOnComplete(() -> log.info("COMPLETED JOB")).subscribe()).subscribe();
        return null;
    }
}

