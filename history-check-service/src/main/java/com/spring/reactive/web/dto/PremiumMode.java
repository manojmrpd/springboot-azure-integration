package com.spring.reactive.web.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PremiumMode {

    private String mode;
    private Integer paymentPerYear;
}
