package com.spring.reactive.web.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ApplicationInfo {
	
	private String applicationNumber;
	private Integer paymentMethod;
	@Digits(integer = 15, fraction = Integer.MAX_VALUE)
	private BigDecimal annualPremium;
	@Digits(integer = 11, fraction = Integer.MAX_VALUE)
	private BigDecimal lumpSumPremium;
	@Digits(integer = 15, fraction = Integer.MAX_VALUE)
	private BigDecimal estimatedTotalPremium;
	private String baseDate;

}
