package com.spring.reactive.web.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(content = Include.NON_EMPTY)
public class PendingPolicyResponse {
	private String result;

}
