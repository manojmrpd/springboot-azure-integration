package com.spring.reactive.web.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.spring.reactive.web.constant.PendingPolicyConstant;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Builder
@Getter
@Setter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Validated
@Component
@ConfigurationProperties(PendingPolicyConstant.HISTORY_CHECK_DATA)
public class HistoryCheckConfig {
	
	@NonNull
	private String uri;
	
	@NonNull
	private Integer retryCount;
	
	@NonNull
	private Integer retryAfter;

}
