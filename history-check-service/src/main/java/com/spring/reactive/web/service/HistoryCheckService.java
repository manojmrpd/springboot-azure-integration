package com.spring.reactive.web.service;

import com.spring.reactive.web.dto.HistoryCheckRequest;
import com.spring.reactive.web.dto.HistoryCheckResponse;

import reactor.core.publisher.Mono;

public interface HistoryCheckService {

	public Mono<HistoryCheckResponse> historyCheck(Mono<HistoryCheckRequest> historyCheckRequest);

}
