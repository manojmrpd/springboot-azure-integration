package com.spring.reactive.web.client;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.spring.reactive.web.client.dto.HistoryCheckRequest;
import com.spring.reactive.web.client.dto.HistoryCheckResponse;
import com.spring.reactive.web.config.property.HistoryCheckConfig;
import com.spring.reactive.web.exception.HistoryCheckException;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

@Component
@Slf4j
@Data
public class HistoryCheckWebClient {

	@Autowired
	private HistoryCheckConfig historyCheckConfig;

	public Mono<HistoryCheckResponse> invokeHistoryCheck(Mono<HistoryCheckRequest> request) {
		return WebClient.create().post().uri(historyCheckConfig.getUri()).contentType(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromPublisher(request, HistoryCheckRequest.class)).retrieve()
				.onStatus(HttpStatus::is5xxServerError, clientResponse -> {
					log.error("500 Internal Server Error. History Check Service failed with Status {}",
							clientResponse.rawStatusCode());
					return Mono.error(new HistoryCheckException("500 Internal Server Error with History Check API"));
				}).onStatus(err -> err.value() == HttpStatus.BAD_REQUEST.value(), clientResponse -> {
					log.error("400 Bad Request. Invalid Request Body to History Check API {}",
							clientResponse.rawStatusCode());
					return Mono.error(new HistoryCheckException("400 Bad Request, Invalid Request Body Sent"));
				}).bodyToMono(HistoryCheckResponse.class)
				.retryWhen(Retry
						.backoff(historyCheckConfig.getRetryCount(),
								Duration.ofMillis(historyCheckConfig.getRetryAfter()))
						.doAfterRetry(retrySignal -> log.error("HISTORYCHECK Call REetried [{}]",
								retrySignal.totalRetries() + 1))
						.filter(throwable -> throwable instanceof HistoryCheckException)
						.onRetryExhaustedThrow((retryBackOffSpec, retrySignal) -> {
							throw new HistoryCheckException(
									"HISTORYCHECK Service call failed to process after max retries");
						}));
	}

}
