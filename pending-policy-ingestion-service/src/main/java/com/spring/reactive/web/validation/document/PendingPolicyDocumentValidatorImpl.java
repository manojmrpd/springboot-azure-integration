package com.spring.reactive.web.validation.document;

import com.spring.reactive.web.constant.PendingPolicyConstant;
import com.spring.reactive.web.document.PendingPolicyDocument;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class PendingPolicyDocumentValidatorImpl implements ConstraintValidator<PendingPolicyDocumentValidator, PendingPolicyDocument> {

    @Override
    public boolean isValid(PendingPolicyDocument pendingPolicyDocument, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean result = true;
        if (Optional.ofNullable(pendingPolicyDocument.getApplicationDate()).isEmpty()) {
            context.buildConstraintViolationWithTemplate(PendingPolicyConstant.APPLICATION_DATE_MUST_NOT_BE_NULL).addConstraintViolation();
            result = false;
        } else if (StringUtils.isNotBlank(pendingPolicyDocument.getApplicationDate()) &&
                !pendingPolicyDocument.getApplicationDate().matches(PendingPolicyConstant.VALID_DATE_REGEX)) {
            context.buildConstraintViolationWithTemplate(PendingPolicyConstant.APPLICATION_DATE_IS_INVALID).addConstraintViolation();
            result = false;
        } else if (pendingPolicyDocument.getPendingPolicyClients().stream().noneMatch(pendingPolicyClientDocument ->
                pendingPolicyClientDocument.getDateOfBirth().matches(PendingPolicyConstant.VALID_DATE_REGEX))) {
            context.buildConstraintViolationWithTemplate(PendingPolicyConstant.BIRTH_DATE_IS_INVALID).addConstraintViolation();
            result = false;
        }
        return result;
    }
}
