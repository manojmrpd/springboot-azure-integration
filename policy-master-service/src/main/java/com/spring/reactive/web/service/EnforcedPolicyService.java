package com.spring.reactive.web.service;

import com.spring.reactive.web.controller.dto.PolicyMasterRequest;
import com.spring.reactive.web.controller.dto.PolicyResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public interface EnforcedPolicyService {

	Flux<PolicyResponse> searchPoliciesByCustomer(final PolicyMasterRequest policyMasterRequest);

    Mono<Void> ingest();
}
