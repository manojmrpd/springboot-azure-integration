package com.spring.reactive.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.reactive.web.service.PendingPolicyService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class PendingPolicyController {

    private final PendingPolicyService pendingPolicyService;

    @PostMapping("/pending-policies")
    public Mono<Void> getPendingPolicies() {
        return pendingPolicyService.getPendingPolicies();
    }

}
