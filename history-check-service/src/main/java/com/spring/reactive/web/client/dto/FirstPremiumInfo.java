package com.spring.reactive.web.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class FirstPremiumInfo {
    @JsonProperty("receiptNumber")
    private String receiptNumber;
    @JsonProperty("receiptDate")
    private String receiptDate;
    @JsonProperty("firstPremiumAmount")
    private String firstPremiumAmount;
    @JsonProperty("firstPremiumPaymentCurrencyCode")
    private String firstPremiumPaymentCurrencyCode;
}