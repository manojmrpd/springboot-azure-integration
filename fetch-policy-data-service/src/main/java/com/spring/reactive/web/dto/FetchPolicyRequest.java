package com.spring.reactive.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@AllArgsConstructor
@Builder(toBuilder = true)
@Setter
@Getter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FetchPolicyRequest {

    private String nameKana;
    private String nameKanji;
    private String dob;
    private Integer gender;
    private Integer policyNumber;
}
