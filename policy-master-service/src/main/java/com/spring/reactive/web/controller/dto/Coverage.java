package com.spring.reactive.web.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import static lombok.AccessLevel.PRIVATE;
import com.fasterxml.jackson.annotation.JsonProperty;
@AllArgsConstructor(access = PRIVATE)
@Builder
@Getter
@Setter
@NoArgsConstructor
public final class Coverage {
    @JsonProperty("maturityExpiryDate")
    private String maturityExpiryDate;
    @JsonProperty("ulPaidUpDate")
    private String ulPaidUpDate;
    @JsonProperty("riderCode")
    private Integer riderCode;
}