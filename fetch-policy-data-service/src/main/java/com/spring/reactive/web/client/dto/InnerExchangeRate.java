package com.spring.reactive.web.client.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class InnerExchangeRate {

    private String currency;
    private String rate;
}
