package com.spring.reactive.web.service;

import com.spring.reactive.web.dto.FetchPolicyRequest;
import com.spring.reactive.web.dto.HistoryCheckResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface PolicyDataService {
    public Mono<HistoryCheckResponse> fetchPolicyDetails(Mono<FetchPolicyRequest> fetchPolicyRequest);
}
