package com.spring.reactive.web.opencsv;

import java.math.BigDecimal;

import com.opencsv.bean.CsvBindByPosition;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IngeniumPolicyResponse {

	@CsvBindByPosition(position = 0)
	private Integer largeTypeCode;
	@CsvBindByPosition(position = 1)
	private String applicationNumber;
	@CsvBindByPosition(position = 2)
	private BigDecimal annualIncome;
	@CsvBindByPosition(position = 3)
	private BigDecimal financialAssets;
	@CsvBindByPosition(position = 4)
	private BigDecimal totalAnnualPremium;
	@CsvBindByPosition(position = 5)
	private BigDecimal estimatedTotalPremium;
	@CsvBindByPosition(position = 6)
	private Long policyCountForOneYear;
	@CsvBindByPosition(position = 7)
	private Long policyCountForTwoYear;
	@CsvBindByPosition(position = 8)
	private Long policyCountForLevelPayment;
	@CsvBindByPosition(position = 9)
	private Long excessPolicyCountForOneYear;
	@CsvBindByPosition(position = 10)
	private Long excessPolicyCountForTwoYear;
	@CsvBindByPosition(position = 11)
	private Long excessPolicyCountForLevelPayment;
	@CsvBindByPosition(position = 12)
	private Integer resultCode1;
	@CsvBindByPosition(position = 13)
	private String errorMessage1;
	@CsvBindByPosition(position = 14)
	private Integer resultCode2;
	@CsvBindByPosition(position = 15)
	private String errorMessage2;
	@CsvBindByPosition(position = 16)
	private Integer resultCode3;
	@CsvBindByPosition(position = 17)
	private String errorMessage3;
	@CsvBindByPosition(position = 18)
	private Integer systemErrorCode;
	@CsvBindByPosition(position = 19)
	private String systemErrorMessage;

}
