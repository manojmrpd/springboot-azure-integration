package com.spring.reactive.web.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder(toBuilder = true)
@ToString
public class FetchPolicyResponse {
    private String nameKana;
    private String dob;
    private Integer gender;
    private String address;
    private Policies policies;
}
