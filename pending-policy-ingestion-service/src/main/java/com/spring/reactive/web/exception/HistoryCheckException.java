package com.spring.reactive.web.exception;

import org.springframework.web.reactive.function.client.WebClientException;

public class HistoryCheckException extends WebClientException {

	private static final long serialVersionUID = 1L;

	public HistoryCheckException(String msg) {
		super(msg);
	}

}
