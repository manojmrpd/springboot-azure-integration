package com.spring.reactive.web.validation.document;

import com.spring.reactive.web.constant.PendingPolicyConstant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = PendingPolicyDocumentValidatorImpl.class)
@Target({ElementType.TYPE})
@Retention(RUNTIME)
@Documented
public @interface PendingPolicyDocumentValidator {

    String message() default PendingPolicyConstant.COM_MANULIFE_AP_VALIDATION_PENDING_POLICY_VALIDATOR_MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}


