package com.spring.reactive.web.controller;

import com.spring.reactive.web.dto.FetchPolicyRequest;
import com.spring.reactive.web.service.PolicyDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.reactive.web.dto.HistoryCheckResponse;

import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1")
@Log4j2
public class FetchPolicyDataController {
	
	@Autowired
	private PolicyDataService policyDataService;

	@PostMapping(value = "/policy-data", consumes = MediaType.APPLICATION_JSON_VALUE, produces =
			MediaType.APPLICATION_JSON_VALUE)
	public Mono<HistoryCheckResponse> fetchPolicyDetails(@RequestBody Mono<FetchPolicyRequest> fetchPolicyRequest) {
		return policyDataService.fetchPolicyDetails(fetchPolicyRequest);
	}

}
